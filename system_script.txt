; ZXGate Systemwide Script v1.0

; Save commands
*save
	Please specify number (save 1..5).
	You can also use short: s1, s2...
	@input
*save 1
*s1
	@save file=SGSAVE1
	@input
*save 2
*s2
	@save file=SGSAVE2
	@input
*save 3
*s3
	@save file=SGSAVE3
	@input
*save 4
*s4
	@save file=SGSAVE4
	@input
*save 5
*s5
	@save file=SGSAVE5
	@input

; Load Commands
*load
	Please specify number (load 1..5)
	You can also use short: l1, l2...
	@input
*load 1
*l1
	@load file=SGSAVE1
	@input
*load 2
*l2
	@load file=SGSAVE2
	@input
*load 3
*l3
	@load file=SGSAVE3
	@input
*load 4
*l4
	@load file=SGSAVE4
	@input
*load 5
*l5
	@load file=SGSAVE5
	@input

; Easter eggs
*sas
	All SAS oelutz!
	@input
*kak eto susno
	Ziga!
	@input
*nurupo
*nullpo
	Gah.
	@input

*what is love
	Baby don't hurt me.
	@input

*jakigan
	Anyone who wishes to live should get out of your way!!
	@input

*eternal force blizzard
*efb
	Your opponent dies.
@input

*ionazun
*magic missile
	Insufficient MP.
@input

*cheat
*xyzzy
*poke
	There are no cheat codes in this game.
	Don't even try looking for any of those.
@input

*mmr
	Wh-what--!?
@input

*neet
	Why don't you work?
@input

*harowa
*hellowork
	Do your best.
@input

*shippai
*shippai shita
*sippai
*sippai sita
*failed
*I failed
	You're still good. Don't give up.
@input

*muripo
*imifu
*mandokusa
*moudamepo
	Try harder.
@input

*neru
*sleep
	Let's sleep.
@input

*format a
*format b
*randomize usr 0
*jmp 0
*rst 0
	Do that and the world will end.
	So don't. There's still so much you have left to do.
@input


