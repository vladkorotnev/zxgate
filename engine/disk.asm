SFLAG_FILE	db "SGSFLAG C",#00
SAVE_FILE   db "SGSLOT0 C",#00
file_error defb #0D,'File not found or bad disk'

	INCLUDE "engine/dzx7_standard.asm"

	INCLUDE "engine/disk.game.asm"
	INCLUDE "engine/disk.sgfs.asm"

SG_COPYSTR:
	LD A, (HL)
	CP 0
	RET Z
	LD (DE), A
	INC DE
	INC HL
	JP SG_COPYSTR


SG_LOAD	; Load script in SCPT_NAME
		
	    push af
	    push bc
	    push de
	    push hl
	  CALL STOP_BGM


	  CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_GRN
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #95 ; Book
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET



	    LD HL, SCPT_LOC ; target address
	    LD (SGFS_ARG_TGT_ADDR), HL

	    LD A, $11 ; target page — default
	    LD (SGFS_ARG_TGT_PAGE),A

	    LD DE, SGFS_ARG_LOOKUP_NAME ; file name
	    LD HL, SCPT_NAME
	    CALL SG_COPYSTR

	    CALL SGFS_LOAD

		EI
		LD HL,SCPT_LOC ; Set script pos to beginning
		LD (SCPT_OFFS),HL

		LD A,0
		OUT (#FE),A

		pop hl
		pop de
		pop bc
		pop af
		RET

SG_LOAD_BGM	; Load music in BGM_NAME
		
	    push af
	    push bc
	    push de
	    push hl
	  	CALL STOP_BGM

	  	CALL ZX_CL_LOWER

	  CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_GRN
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #94 ; Note
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

		LD HL, Z7BUFF ; target address
	    LD (SGFS_ARG_TGT_ADDR), HL

	    LD A, $10 ; target page — default
	    LD (SGFS_ARG_TGT_PAGE),A

	    LD DE, SGFS_ARG_LOOKUP_NAME ; file name
	    LD HL, BGM_NAME
	    CALL SG_COPYSTR

	    CALL SGFS_LOAD

	    ; decompress music after loading:
	
				
		LD HL,Z7BUFF
		LD DE,BGMFILE
		CALL dzx7_standard

		IFDEF USE_PAGES_FOR_MUSIC
			DI
			ld a,$11
			ld bc,$7ffd
			OUT (C), A
		ENDIF

		EI
		LD A,0
		OUT (#FE),A

		pop hl
		pop de
		pop bc
		pop af
		LD A,1:INC A:DEC A ; ret NZ
		RET

SG_LOAD_GRAPH	; Load picture in GRPH_NAME
		
	    push af
	    push bc
	    push de
	    push hl
	  	CALL PAUSE_BGM

		CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_GRN
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #96 ; Picture
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

	     LD A,0
		OUT (#FE),A

		LD HL, Z7BUFF ; target address
	    LD (SGFS_ARG_TGT_ADDR), HL

	    LD A, $11 ; target page — default
	    LD (SGFS_ARG_TGT_PAGE),A

	    LD DE, SGFS_ARG_LOOKUP_NAME ; file name
	    LD HL, GRPH_NAME
	    CALL SG_COPYSTR

	    CALL SGFS_LOAD

		LD A,4
		OUT (#FE),A


		DI
		LD HL,Z7BUFF
		LD DE,16384
		CALL dzx7_standard


		EI
		HALT

		LD A,0
		OUT (#FE),A
		CALL RESTART_BGM

		pop hl
		pop de
		pop bc
		pop af
		RET



TRDOS	EQU #3D13