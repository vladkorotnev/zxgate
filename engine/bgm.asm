STOP_BGM	XOR A
			LD (BGM_ON),A
PAUSE_BGM	DI
			LD A,63
			LD I,A
			IM 1
			IFDEF USE_PAGES_FOR_MUSIC
				DI
				ld a,$10
				ld bc,$7ffd
				OUT (C), A
			ENDIF
			CALL PLAYER+8
			IFDEF USE_PAGES_FOR_MUSIC
				DI
				ld a,$11
				ld bc,$7ffd
				OUT (C), A
			ENDIF
			EI
			RET

RESTART_BGM 
			PUSH AF
			PUSH HL
			PUSH DE
			PUSH BC
			LD A,(BGM_ON)
			CP 1
			JP Z,_do_restart
       		POP BC
       		POP DE
       		POP HL
       		POP AF
			RET
START_BGM	
			PUSH AF
			PUSH HL
			PUSH DE
			PUSH BC
			IFDEF USE_PAGES_FOR_MUSIC
				DI
				ld a,$10
				ld bc,$7ffd
				OUT (C), A
			ENDIF
			CALL PLAYER
			IFDEF USE_PAGES_FOR_MUSIC
				DI
				ld a,$11
				ld bc,$7ffd
				OUT (C), A
			ENDIF
_do_restart	
			DI
			LD   A,24      ; means JR
			LD   (65535),A
			LD   A,195     ; means JP
			LD   (65524),A
			LD   HL,BGM_ISR    
			LD   (65525),HL
			LD   HL,#FE00
			LD   DE,#FE01
			LD   BC,#0100
			LD   (HL),#FF
			LD   A,H
			LDIR
			DI
			LD   I,A
			IM   2
       		LD A,1
       		LD (BGM_ON),A
	     	 EI
			POP BC
       		POP DE
       		POP HL
       		POP AF
			RET

BGM_ISR    DI
			push hl,de,bc,af
			exx
			exa
			push hl,de,bc,af
       IFDEF USE_PAGES_FOR_MUSIC
				DI
				ld a,$10
				ld bc,$7ffd
				OUT (C), A
		ENDIF
       CALL PLAYER+5 
       DI
			IFDEF USE_PAGES_FOR_MUSIC
				DI
				ld a,$11
				ld bc,$7ffd
				OUT (C), A
			ENDIF
		pop af,bc,de,hl
		exx:exa
		pop af,bc,de,hl
       EI      
       RET   

PLAYER EQU #C000
BGMFILE EQU #C86E

