SG_LOOKUP_LOC EQU #C000
SG_LOOKUP_FIL db "GAMEDAT_C",#00

SGFS_ARG_LOOKUP_NAME db "SGBOOT  C", #00
SGFS_ARG_TGT_PAGE db $11
SGFS_ARG_TGT_ADDR dw #0

SG_VOLUME_NAM db "GAMEDAT"
SG_VNAME_PH db "0C", #00
SG_VOL_LDLEN dw #00
SG_VOL_SS dw #00

SGFS_FIND_ZERO_HL
	LD A,(HL)
	CP #0
	RET Z
	INC HL
	JP SGFS_FIND_ZERO_HL

SGFS_LOAD
	; Load arbitrary file from the library into memory
	; Before calling: set SGFS_ARG* !

	LD DE, SGFS_ARG_LOOKUP_NAME
	LD HL, SG_LOOKUP_LOC ; map location

		DI
		ld a,$13 		; Switch #C000..FFFF to Page 2!
		ld bc,$7ffd
		OUT (C), A
		
		

_sgfs_lkup_loop ; iterate over map
		CALL StrictStrCmp ; Compare null-terminated requested string and current map entry
		JP Z, _sgfs_found
		CALL SGFS_FIND_ZERO_HL
		; Now: ->00 ?? ?? ?? FN where FN is new file name
		INC HL   
		; Now: 00 ->?? ?? ?? FN where FN is new file name
		 INC HL 
		 ; Now: 00 ?? ->?? ?? FN where FN is new file name
		  INC HL 
		   ; Now: 00 ?? ?? ->?? FN where FN is new file name
		   INC HL 
		   ; Now: 00 ?? ?? ?? ->FN where FN is new file name
		LD DE, SGFS_ARG_LOOKUP_NAME ; reset file name
		JP _sgfs_lkup_loop
_sgfs_found
		CALL SGFS_FIND_ZERO_HL ; forward to end of file name, just in case
		
		; next byte — volume number
		INC HL 
		LD A, (HL)
		ADD A,#30 ; convert to ASCII number
		; place into volume file name
		LD (SG_VNAME_PH),A
		
		; next byte - start sector
		INC HL 
		LD A, (HL)
		LD (SG_VOL_SS),A
		
		; next byte - length in sectors
		INC HL 
		LD A, (HL)
		LD (SG_VOL_LDLEN),A
		
		; switch to requested page
		DI
		LD A, (SGFS_ARG_TGT_PAGE)
		LD BC, $7ffd
		OUT (C),A
		EI
		; end switch
		; Reset the BDI
		LD A,0
		LD C,0
		CALL TRDOS
		LD A,1
		OUT (#FE),A
		; Select drive A
		LD C,1
		LD A,0
		CALL TRDOS
		LD A,2
		OUT (#FE),A
		; Setup for floppy
		LD C,#18 
	    CALL TRDOS
	    LD A,3
		OUT (#FE),A
	    ; Set file name
	    LD HL,SG_VOLUME_NAM
	    LD C,#13
	    CALL TRDOS
	    LD A,4
		OUT (#FE),A
	    ; Find file descriptor
	    LD HL, #5D06
	    LD (HL), 9 ; compare only 9 bytes (filename)
	    LD C,#0A
	    CALL TRDOS 
	    ; Read file descriptor
	    LD A,C
	    LD C,#08
	    CALL TRDOS
	    LD A,5
		OUT (#FE),A
	    ; Load blocks
	    LD DE, (#5CEB) ; track/sect no.

	    ; begin add SS to current track/sect no.
	    LD A, (SG_VOL_SS)
	    ADD A,0 ; update flags
	    JP Z, _ss_recalc_no_need

	    ; tr-dos has only 16 sect per trk, so need to
	    ; D = D + SS / 16
	    ; E = E + SS % 16
	    ; but what if E was already marginally high and we rolled over again?
	    ; this will fix it just to be sure:
	    ; D = D + E / 16 (if we didn't rollover, essentially D+0)
	    ; E = E % 16
	    
	    ; #1: SS / 16 -> essentially SS>>4 and remove whatever trash could have appeared with a bit mask
	    RRA : RRA : RRA : RRA : AND #F
	    ; D = D + (SS/16)
	    ADD A, D : LD D, A

	    ; get SS again
	    LD A, (SG_VOL_SS)

	    ; #2: SS % 16 is essentially a bit mask
	    AND #F
	    ; E = E + (SS%16)
	    ADD A, E : LD E, A

	    ; #3: D = D+ E/16 (we already have E in A)
	    ; E/16 same way
	    RRA : RRA : RRA : RRA : AND #F
	    ; D = D + (E/16)
	    ADD A, D : LD D, A
	    ; restore E into A
	    LD A, E
	    ; E % 16
	    AND #F
	    ; E = (E%16)
	    LD E, A


    ; end add SS
	    
_ss_recalc_no_need
	    LD A, (SG_VOL_LDLEN)
	    LD B, A
	    XOR A

		LD C, #05 ; func. load block of sectors
		LD HL, (SGFS_ARG_TGT_ADDR)

		CALL TRDOS
		LD A,6
		OUT (#FE),A
		RET

; ------------------------------------------------------ 


SGFS_INIT_LOOKUP_TABLE
	CALL BOTTOM_ICO_GRN	
	  CALL ZX_CL_LOWER
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

		DI
		ld a,$13 		; Switch #C000..FFFF to Page 2!
		ld bc,$7ffd
		OUT (C), A
		EI
	    LD A,0
		OUT (#FE),A

		CALL 15649 ; Init TRDOS
	    ; Reset the BDI
		LD A,0
		LD C,0
		CALL TRDOS
		LD A,1
		OUT (#FE),A

		LD C,1
		LD A,0
		CALL TRDOS ; set drive A

		LD A,2
		OUT (#FE),A

	   LD C,#18 ; get ready for floppy
	   CALL TRDOS

	   	LD A,3
		OUT (#FE),A

	    LD HL,SG_LOOKUP_FIL
	    LD C,#13
	    CALL TRDOS ; set file name

	    LD HL,#5CF9 ; TR-DOS Operation Type
		XOR A
		LD (HL), A ; 00 = read
		LD HL,#5D10 
		LD (HL),A
		LD HL,SG_LOOKUP_LOC ; target address
		LD A,#03 ; Mode=Read full to HL
		LD C,#0E ; Op=Read
		CALL TRDOS

		LD A,0
		OUT (#FE),A

		DI
		ld a,$11 		; Switch #C000..FFFF to Page 1!
		ld bc,$7ffd
		OUT (C), A
		EI

		RET