; ZXGate Command Set 


; Script command convention:
	; ------------ Conditionals
	; D1 aa bb aa ... 00		Check GAMEFLAGS (aa is offset, bb is value, logic AND)
	; D0 aa bb					Set GAMEFLAGS (aa is offset, bb is value)
	; D2 aa 					Set TEMPFLAGS (logic OR, every bit is flag)
	; D3 aa 					Check TEMPFLAGS (logic AND)
	; D4 aa 					Set SYSFLAG (logic OR, every bit is flag)
	; D5 aa 					Check SYSFLAG
	; F2						Else (if Check not matched)
	; F3						End Check

	; ------------ Multi-media
	; E0 xx xx xx ... 00		Load Script File
	; E1 xx xx xx ... 00		Load Music File
	; E2 xx xx xx ... 00		Load Image File
	; E3 xx xx xx ... 00		Reserved
	; F4						Stop Music

	; ------------ Gameplay
	; F0 xx xx xx ... 00		Save to file
	; F1 xx xx xx ... 00		Load from file
	; E4						Game Over
	; F5 xx xx xx ... 00		Reserved
	; E5 xx						Wait xx seconds
	; F6						Clear screen
	; F7						EOF
	; FF						User Input Pointer
	; FD xx xx xx ... 00
	;    xx xx xx ... 00
	;	 xx xx xx ... 00 FE 	Pointer List
	; xx xx xx xx ... 00 		Print string
	; Custom Characters: see CUSTOM_CHARS above

; -------- Game Flag / Commands with binary arguments
SCMD_SETFLAG 	equ #D0
SCMD_GETFLAG 	equ #D1
SCMD_SETTF		equ #D2
SCMD_GETTF		equ #D3
SCMD_SETSYS		equ #D4
SCMD_GETSYS		equ #D5
; -------- Game UI / BGM / Commands with string arguments
SCMD_GOTO		equ #E0
SCMD_BGM		equ #E1
SCMD_DRAW		equ #E2
SCMD_GAMEOVER	equ #E4
SCMD_WAIT		equ #E5
; -------- Save/Load / Small Commands with byte or no arguments
SCMD_SAVE		equ #F0
SCMD_LOAD		equ #F1
SCMD_ELSE		equ #F2
SCMD_ENDIF		equ #F3
SCMD_STOPBGM	equ #F4
SCMD_CLRSCR		equ #F6
SCMD_INPUT		equ #FF
; -------- Misc.
SCMD_PTRLIST	equ #FD
SCMD_PTREND		equ #FE
SCMD_EOF		equ #F7