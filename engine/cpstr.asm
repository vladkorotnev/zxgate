StrictStrCmp:
	; Load next chars of each string
	ld a, (de)
	ld b, a
	ld a, (hl)
	; Compare
	cp b
	; Return non-zero if chars don't match
	ret nz
	; Check for end of both strings
	cp #00
	; Return if strings have ended
	ret z
	; Otherwise, advance to next chars
	inc hl
	inc de
	jr StrictStrCmp