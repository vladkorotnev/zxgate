SG_SAVE_GAME
	push af
	    push bc
	    push de
	    push hl
	    CALL PAUSE_BGM

	  CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_RED
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #97 ; Joystick
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

	    LD A,0
		OUT (#FE),A

		CALL 15649 ; Init TRDOS

	    ; Reset the BDI
		LD A,0
		LD C,0
		CALL TRDOS
				
		LD A,1
		OUT (#FE),A

		LD C,1
		LD A,0
		CALL TRDOS ; set drive A

		LD A,2
		OUT (#FE),A

	   LD C,#18 ; get ready for floppy
	   CALL TRDOS

	    LD HL,SAVE_FILE
	    LD C,#13
	    CALL TRDOS ; set file name

		LD A,3
		OUT (#FE),A
	    LD C,#12 ; DELETE file
	    CALL TRDOS

	    LD A,4
		OUT (#FE),A

	    LD HL,SAVE_FILE
	    LD C,#13
	    CALL TRDOS ; set file name

	    LD HL,START_GAMEDATA
	    LD DE,END_GAMEDATA - START_GAMEDATA
	    LD C,#0B ; SAVE file
	    call TRDOS

		LD A,0
		OUT (#FE),A

		CALL RESTART_BGM
	 	pop hl
		pop de
		pop bc
		pop af
		RET

SG_LOAD_GAME 
		push af
	    push bc
	    push de
	    push hl
	  CALL STOP_BGM

	  CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_GRN
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #97 ; Flag
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

	     LD A,0
		OUT (#FE),A

		CALL 15649 ; Init TRDOS

	    ; Reset the BDI
		LD A,0
		LD C,0
		CALL TRDOS
				
		LD A,1
		OUT (#FE),A

		LD C,1
		LD A,0
		CALL TRDOS ; set drive A

		LD A,2
		OUT (#FE),A

	   LD C,#18 ; get ready for floppy
	   CALL TRDOS

	   	LD A,3
		OUT (#FE),A

	    LD HL,SAVE_FILE
	    LD C,#13
	    CALL TRDOS ; set file name

	    LD A,0
		OUT (#FE),A

		 ld c,#0a
		  call TRDOS ; check file exists
		    ld a,c
		    inc c
		    jp z,_savefile_not_found 

		LD HL,#5CF9 ; TR-DOS Operation Type
		XOR A
		LD (HL), A ; 00 = read
		LD HL,#5D10 
		LD (HL),A
		LD HL,START_GAMEDATA ; target address
		LD A,#03 ; Mode=Read full to HL
		LD C,#0E ; Op=Read
		CALL TRDOS

		LD A,4
		OUT (#FE),A

		LD A,0
		OUT (#FE),A

		; Reloaded game, redraw graphic
		CALL SG_LOAD_GRAPH

		pop hl
		pop de
		pop bc
		pop af
		LD A,1
		INC A:DEC A ; returning NZ, because load ok
		RET
_graphfile_not_found 
	CALL RESTART_BGM
_savefile_not_found
	CALL ZX_CL_LOWER
	ld HL,file_error-1
	ld b,27
_SFNF_PRN 		INC HL
				LD A,(HL)
				PUSH BC
				PUSH HL
				CALL ZX_PRINT
				POP HL
				POP BC
				HALT
				DJNZ _SFNF_PRN
		CALL SG_WAIT_KEY
		pop hl
		pop de
		pop bc
		pop af
		XOR A ; returning Z, because file-not-found
		RET 


SG_READ_SYSFLAGS ; Read SYSFLAGS from file to RAM
		push af
	    push bc
	    push de
	    push hl
	  

	  CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_GRN
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #9A ; Flag
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

	     LD A,0
		OUT (#FE),A

		CALL 15649 ; Init TRDOS

	    ; Reset the BDI
		LD A,0
		LD C,0
		CALL TRDOS
				
		LD A,1
		OUT (#FE),A

		LD C,1
		LD A,0
		CALL TRDOS ; set drive A

		LD A,2
		OUT (#FE),A

	   LD C,#18 ; get ready for floppy
	   CALL TRDOS

	   	LD A,3
		OUT (#FE),A

	    LD HL,SFLAG_FILE
	    LD C,#13
	    CALL TRDOS ; set file name

	    LD A,0
		OUT (#FE),A

	;	 ld c,#0a
	;	  call TRDOS ; check file exists
	;	    ld a,c
	;	    inc c
	;	    jp z,_no_syflag_file 


		LD HL,#5CF9 ; TR-DOS Operation Type
		XOR A
		LD (HL), A ; 00 = read
		LD HL,#5D10 
		LD (HL),A
		LD HL,SYSFLAGS ; target address
		LD A,#03 ; Mode=Read full to HL
		LD C,#0E ; Op=Read
		CALL TRDOS

		LD A,4
		OUT (#FE),A

		LD A,0
		OUT (#FE),A

_no_syflag_file		pop hl
		pop de
		pop bc
		pop af
		RET

SG_SAVE_SFLAGS ; Saves SYSFLAGS to a respective file
		push af
	    push bc
	    push de
	    push hl
	    CALL PAUSE_BGM

	  CALL ZX_CL_LOWER
	  CALL BOTTOM_ICO_RED
	  LD A, #93 ; Floppy
	  CALL ZX_PRINT
	  LD A, #9A ; Flag
	  CALL ZX_PRINT
	  CALL BOTTOM_ATTR_RESET

	    LD A,0
		OUT (#FE),A

		CALL 15649 ; Init TRDOS

	    ; Reset the BDI
		LD A,0
		LD C,0
		CALL TRDOS
				
		LD A,1
		OUT (#FE),A

		LD C,1
		LD A,0
		CALL TRDOS ; set drive A

		LD A,2
		OUT (#FE),A

	   LD C,#18 ; get ready for floppy
	   CALL TRDOS

	   	

	    LD HL,SFLAG_FILE
	    LD C,#13
	    CALL TRDOS ; set file name

		LD A,3
		OUT (#FE),A
	    LD C,#12 ; DELETE file
	    CALL TRDOS

	    LD A,4
		OUT (#FE),A

	    LD HL,SFLAG_FILE
	    LD C,#13
	    CALL TRDOS ; set file name

	    LD HL,SYSFLAGS
	    LD DE,1 ; 1 byte long
	    LD C,#0B ; SAVE file
	    call TRDOS

		LD A,0
		OUT (#FE),A

		CALL RESTART_BGM
	 	pop hl
		pop de
		pop bc
		pop af
		RET