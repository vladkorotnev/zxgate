RESET_FX  ; An effect mimicking RANDOMIZE USR 0
		; Clear screen
		CALL CLS
		; Fill screen with red ink black paper
		LD A,2
		CALL SET_ATTR
		; Set border to gray
		LD A,7
		OUT (254),A
		LD HL,SCREEN_AREA + SCREEN_SZ

		; First fill the screen upwards
		LD A,24 ; Outer loop, 24 times
_RS_L0		LD B,255 ; Inner loop, 255 times
_RS_L1				LD (HL),2 ; Pixel data: PPPPPPPI 
				DEC HL
			DJNZ _RS_L1
			DEC A
			CP 0
		JR NZ, _RS_L0
		; Clear up the remaining dots
		LD B,24 ; 
_RS_L5				LD (HL),2; Pixel data: PPPPPPPI
				DEC HL
			DJNZ _RS_L5
		; Wait 5 frames
		LD B,5
;_RS_WAIT HALT
;		DJNZ _RS_WAIT
		; ... then downwards
		LD HL,SCREEN_AREA
		LD A,24 ; Outer loop, 24 times
_RS_L2		LD B,255 ; Inner loop, 255 times
_RS_L3				LD (HL),0 ; Pixel data: PPPPPPPP
				INC HL
				CALL _WASTE
			DJNZ _RS_L3
		DEC A
		CP 0
		JR NZ, _RS_L2
		; Clear up the remaining dots
		LD B,24 ; 
_RS_L4		LD (HL),0 ; Pixel data: PPPPPPPP
			INC HL
		DJNZ _RS_L4
		; Some useless stuff to waste time
		CALL _WASTE
		; Wait some frames
		LD B,50
_RS_WAIT2 HALT
		DJNZ _RS_WAIT2
		; Fill screen with default attr
		LD A,56
		CALL SET_ATTR
		LD DE,PROMPT
		LD BC,eoprompt-PROMPT
		CALL 8252
		RET

RESET_FX_INPUT_MODE 
		CALL CLS
		LD A,"L"
		RST 16
		LD HL,23264
		LD (HL),184
		LD (cursor_attr_pos),HL
		RET

RESET_FX_PUT_LETTER
		LD C,A
		LD A,23
		OUT (#FE),A
		LD A,C
		RST 16
		LD A,"L"
		RST 16
		LD A,8
		RST 16
		LD HL,(cursor_attr_pos)
		LD (HL),56
		LD A,L ; HL for attrs is always 5AE0 to 5AFF otherwise out of screen
		CP #FF
		JP NZ,_NOSCRL
		LD L,#DF ; If we scrolled, reset to beginning of line (5AE0)
_NOSCRL	INC HL
		LD (HL),184
		LD (cursor_attr_pos),HL
		LD A,7
		OUT (#FE),A
		RET

RESET_FX_PUT_STR ; HL Must point to the string ending with 0
		PUSH HL
		CALL CLS
		POP HL
RESET_FX_PUT_STR_LOOP		LD A,(HL)
							CP 0
							RET Z
							PUSH HL
							CALL RESET_FX_PUT_LETTER
							POP HL
							INC HL
		LD B,5
_RPLW	HALT
		DJNZ _RPLW
							JP RESET_FX_PUT_STR_LOOP
		RET

PROMPT defb 22,21,0,#7F," 2016 Genjitsu Research",13
eoprompt equ $
cursor_attr_pos defw 23264

_WASTE  PUSH HL
		POP HL

		RET
