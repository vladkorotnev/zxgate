	DEFINE ATTR_AREA 22528
	DEFINE ATTR_SZ 767
	DEFINE SCREEN_AREA 16384
	DEFINE SCREEN_SZ 6144
; -------------------------------------------------------------------
DEF_ATTR
          LD A,%00111000
          JP SET_ATTR
; ------------------------------------------------------------------- 
CLR_ATTR
		LD A,0
		JP SET_ATTR
; -------------------------------------------------------------------		
SET_ATTR ; -- A must be the attribute to fill
		LD HL,ATTR_AREA
		LD B,255
CLLP1	LD (HL),A ; Copy 0 to screen color area
		INC HL
		DJNZ CLLP1
		LD B,255
CLLP2	LD (HL),A ; Copy 0 to screen color area
		INC HL
		DJNZ CLLP2
		LD B,255
CLLP3	LD (HL),A ; Copy 0 to screen color area
		INC HL
		DJNZ CLLP3
		LD B,3
CLLP4	LD (HL),A ; Copy 0 to screen color area
		INC HL
		DJNZ CLLP4
		RET

; -------------------------------------------------------------------
CLS		EQU #0D6B
; -------------------------------------------------------------------

DOWN_HL:
     INC H
     LD A,H
     AND 7
     RET NZ ;CY=0
     LD A,L
     ADD A,32
     LD L,A
     RET C  ;CY=1
     LD A,H
     ADD A,-8
     LD H,A     ;CY=1
     RET
; -------------------------------------------------------------------
UP_HL:
     LD A,H
     DEC H
     AND 7
     RET NZ
     LD A,L
     SUB 32
     LD L,A
     RET C
     LD A,H
     ADD A,8
     LD H,A
     RET