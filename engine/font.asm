CUSTOM_CHARS
	LD DE, 65368 ; USR "A"
	LD HL, USR_A
	LD BC, 11*8 
	LDIR
	RET

USR_A defb #01, #3E, #44, #44, #44, #44, #3B, #00 ; Char code #90 = Alpha
USR_B defb #00, #38, #44, #58, #44, #78, #40, #40 ; Char code #91 = Betta
USR_C defb #00, #01, #11, #21, #7F, #20, #10, #00 ; Char code #92 = Enter
USR_D defb #3F, #5D, #9D, #81, #99, #99, #81, #FF ; Char code #93 = Floppy
USR_E defb #1F, #11, #11, #11, #13, #37, #73, #20 ; Char code #94 = Note
USR_F defb #66, #99, #D5, #D5, #91, #D7, #38, #00 ; Char code #95 = Book
USR_G defb #FF, #83, #91, #A3, #F7, #FF, #FF, #FF ; Char code #96 = Picture
USR_H defb #18, #3C, #3C, #FF, #99, #99, #83, #FF ; Char code #97 = Joystick
USR_J defb #3C, #42, #91, #91, #9D, #81, #42, #3C ; Char code #98 = Clock
USR_K defb #18, #18, #3C, #FF, #7E, #3C, #66, #C3 ; Char code #99 = Star
USR_L defb #B0, #F9, #FF, #FF, #FF, #CF, #86, #80 ; Char code #9A = Flag