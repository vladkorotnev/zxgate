
; ------------------------------------------------------------------
DISAPPEAR_INIT	
			LD A,127 ; Reset mask to all on
			LD (DISAPPEAR_MSK),A
			RET
; -------------------------------------------------------------------
DISAPPEAR_STEP 
			LD A,(DISAPPEAR_MSK) ; Current color mask
			OR A ; Clear carry
			RRA ; Shift mask right
			LD (DISAPPEAR_MSK),A ; Save mask for next step
			LD HL,ATTR_AREA ; Beginning of screen color memory

			LD B,255 ;  pass of 255 items
_DISAP_L1	LD A,(DISAPPEAR_MSK) ; Copy mask to accumulator
			LD C,(HL) ; Copy current byte of original
			AND C ; Bitwise and of mask and original
			; Now A contains the resulting attribute
			LD (HL),A ; Write it to the screen
			INC HL
			DJNZ _DISAP_L1
			LD B,255 ;  pass of 255 items
_DISAP_L2	LD A,(DISAPPEAR_MSK) ; Copy mask to accumulator
			LD C,(HL) ; Copy current byte of original
			AND C ; Bitwise and of mask and original
			; Now A contains the resulting attribute
			LD (HL),A ; Write it to the screen
			INC HL
			DJNZ _DISAP_L2
			LD B,255 ;  pass of 255 items
_DISAP_L3  LD A,(DISAPPEAR_MSK) ; Copy mask to accumulator
			LD C,(HL) ; Copy current byte of original
			AND C ; Bitwise and of mask and original
			; Now A contains the resulting attribute
			LD (HL),A ; Write it to the screen
			INC HL
			DJNZ _DISAP_L3
			LD B,3 ;  pass of 255 items
_DISAP_L4   LD A,(DISAPPEAR_MSK) ; Copy mask to accumulator
			LD C,(HL) ; Copy current byte of original
			AND C ; Bitwise and of mask and original
			; Now A contains the resulting attribute
			LD (HL),A ; Write it to the screen
			INC HL
			DJNZ _DISAP_L4
_DISAP_DONE	RET


; -------------------------------------------------------------------

DISAPPEAR_MSK db 0

