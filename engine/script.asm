;	INCLUDE "engine/commandset.asm"
; it is already included in MAIN

SG_RUN_FROM_START
		; Reset Temp Flags for new scene
		LD HL, TEMPFLAGS
		LD (HL), #00
SG_RUN_FROM_LOAD
		CALL BOTTOM_ATTR_RESET
		LD HL,SCPT_LOC ; Set script pos to beginning
		LD (SCPT_OFFS),HL

SG_RUN  LD HL,(SCPT_OFFS) ; Read current pos in memory
_SG_RUNNING
		LD A, (HL) ; Read current symbol
		CP #D0	; above D0 symbols are reserved as system directives
		JR C, _SG_PRINT_LINE
		; If we end up here, it's a system directive..
			CP SCMD_SETFLAG ; Directive #D0 aa bb means "set bb in flag aa"
			JP Z,_SG_SET_GAMEFLAG
			CP SCMD_GETFLAG ; Directive #D1 aa bb .. .. 00 means "check every flag aa to be equal to bb"
			JP Z,_SG_GET_GAMEFLAG
			CP SCMD_SETTF ; Directive #D2 xx means "set tempflag"
			JP Z,_SG_SET_TEMPFLAG
			CP SCMD_GETTF ; Directive #D3 xx means "check tempflags by mask"
			JP Z,_SG_CHK_TEMPFLAG
			CP SCMD_SETSYS ; Directive #D4 xx means "set sysflag"
			JP Z,_SG_SET_SYSFLAG
			CP SCMD_GETSYS ; Directive #D5 xx means "check sysflag"
			JP Z,_SG_CHK_SYSFLAG

			CP SCMD_GOTO ; Directive #E0 means Goto file
			JP Z,_SG_LOAD_FILE
			CP SCMD_BGM ; Directive #E1 means load BGM file
			JP Z,_SG_LOAD_BGM
			CP SCMD_DRAW ; Directive #E2 means draw screen
			JP Z,_SG_DRAW_SCREEN
			CP SCMD_GAMEOVER ; Directive #E4 means game over
			JP Z,_SG_GAME_OVER
			CP SCMD_WAIT ; Direvtive #E5 means wait seconds
			JP Z,_SG_WAIT

			CP SCMD_SAVE ; Directive #F0 means save
			JP Z,_SG_SAVE
			CP SCMD_LOAD ; Directive #F1 means load
			JP Z, _SG_LOAD
			CP SCMD_ELSE ; Directive #F2 means else
			JP Z,_SG_ELSE
			CP SCMD_ENDIF ; Directive #F3 means end if
			JP Z,_SG_ENDIF
			CP SCMD_STOPBGM ; Directive #F4 means stop BGM
			JP Z,_SG_STOP_BGM
			CP SCMD_CLRSCR ; Directive #F6 means clear screen
			JP Z,_SG_CLEAR_SCREEN

			CP SCMD_PTRLIST ; Directive #FD is pointer list begin, used for input search
			JP Z,_SG_FIND_END_OF_PTR

			CP SCMD_EOF ; Directive #F7 is EOF, used for input search
			JP Z,_SG_NEXT_CHAR
			CP SCMD_INPUT ; Directive #FF means user input
			JP Z,_SG_USER_INPUT
			; ... TODO: Other system directives
		JP _SG_NEXT_CHAR

_SG_PRINT_LINE ; Print text line until #00 reached
		CALL SG_CLEAR_BOTTOM
_SG_PRINTING_LINE
		PUSH HL
		PUSH AF
	;	CALL SG_TICK_EFFECT
		CALL ZX_PRINT
		
			LD BC, 32766 ; B, N, M, Symbol Shift, Space
			IN A,(C) 
			RRA : RRA  ; NC is now SS-pressed
			JP NC, _sgp_nohalt
			LD BC, 65278 ; V C X Z CS
			IN A,(C) 
			RRA ; NC is now CS-pressed
			JP NC, _sgp_nohalt

			HALT

_sgp_nohalt		POP AF
		POP HL
		; get next char
		INC HL
		LD A,(HL)
		CP #00 ; Check is it #00?
		JR NZ, _SG_PRINTING_LINE ; print on if not #00
		CALL SG_WAIT_KEY
		CALL SG_CLEAR_BOTTOM
_SG_NEXT_CHAR ; Proceed to next symbol
		INC HL
		LD (SCPT_OFFS),HL
		JP _SG_RUNNING

_SG_GAME_OVER
		CALL STOP_BGM
		LD A,%00111000 ; RESET Attributes for bottom screen
		LD (#5C48),A
		LD (#5C8D),A
		LD (#5C8F),A
		CALL RESET_FX
		DUP 50
		HALT
		EDUP
		CALL RESET_FX_INPUT_MODE
		DUP 25
		HALT
		EDUP
		LD HL,STR_GAME_OVER
		CALL RESET_FX_PUT_STR
		DUP 50
		HALT
		EDUP
		; Reload default script name
		LD HL,STRT_SCPT
		LD DE,SCPT_NAME
		LD BC,9
		LDIR
		CALL SG_LOAD
		JP SG_RUN_FROM_START


_SG_LOAD_BGM
		; Load BGM name from script
		; get next char
		LD DE, BGM_NAME
_SG_LDING_BGM
		INC HL
		LD A,(HL)
		CP #00 ; Check is it #00?
		JR Z, _SG_DO_BGM ; if it is, move on
		LD (DE), A ; copy character otherwise
		INC DE
		JP _SG_LDING_BGM
_SG_DO_BGM 
		CALL SG_LOAD_BGM
		JP Z,_SG_NEXT_CHAR ; if load fail, don't start music
		CALL START_BGM
		JP _SG_NEXT_CHAR

_SG_SAVE
		; save file from script
		LD DE, SAVE_FILE
_SG_SAVING
		INC HL
		LD A,(HL)
		CP #00 ; Check is it #00?
		JR Z, _SG_DO_SAVE ; if it is, move on
		LD (DE), A ; copy character otherwise
		INC DE
		JP _SG_SAVING
_SG_DO_SAVE 
		CALL SG_SAVE_GAME
		JP _SG_NEXT_CHAR

_SG_LOAD
		; Load file from script
		LD DE, SAVE_FILE
_SG_LOADING
		INC HL
		LD A,(HL)
		CP #00 ; Check is it #00?
		JR Z, _SG_DO_LOAD ; if it is, move on
		LD (DE), A ; copy character otherwise
		INC DE
		JP _SG_LOADING
_SG_DO_LOAD 
		CALL SG_LOAD_GAME
		JP Z,_SG_NEXT_CHAR; if loading failed
		CALL SG_LOAD
		JP SG_RUN_FROM_LOAD

_SG_DRAW_SCREEN
		LD DE, GRPH_NAME
_SG_LDING_SCR
		INC HL
		LD A,(HL)
		CP #00 ; Check is it #00?
		JR Z, _SG_DO_SCR ; if it is, move on
		LD (DE), A ; copy character otherwise
		INC DE
		JP _SG_LDING_SCR
_SG_DO_SCR
		CALL SG_LOAD_GRAPH
		JP _SG_NEXT_CHAR


_SG_STOP_BGM
		PUSH HL
		PUSH AF
		CALL STOP_BGM
		POP AF
		POP HL
		JP _SG_NEXT_CHAR


_SG_LOAD_FILE
		LD DE, SCPT_NAME
_SG_LDING_FILE
		INC HL
		LD A,(HL)
		CP #00 ; Check is it #00 end-of-string?
		JR Z, _SG_DO_FILE ; if it is, move on
		LD (DE), A ; copy character otherwise
		INC DE
		JP _SG_LDING_FILE
_SG_DO_FILE
		CALL SG_LOAD
		JP SG_RUN_FROM_START


_SG_CLEAR_SCREEN
		PUSH HL
			CALL DISAPPEAR_INIT
			EI
			DUP 7
			  	HALT
			  	CALL DISAPPEAR_STEP
			EDUP
			  CALL CLS
		POP HL
		JP _SG_NEXT_CHAR


_SG_WAIT
		PUSH BC
		INC HL ; Get next byte into B register
		LD B, (HL)
		PUSH BC
		PUSH HL
		CALL BOTTOM_ICO_GRN ; Draw green timer icon
		LD A,#98
		CALL ZX_PRINT
		CALL BOTTOM_ATTR_RESET
		POP HL
		POP BC
_SG_Waiting 
		EI
		DUP 50 ; 50 halt = 1 second
		HALT
		EDUP
		DJNZ _SG_Waiting ; Dec B, if not 0 wait more
		JP _SG_NEXT_CHAR

_SG_FIND_END_OF_PTR
		INC HL
		LD A,(HL)
		CP SCMD_PTREND ; FE is end of ptr list
		JP NZ,_SG_FIND_END_OF_PTR
		JP _SG_NEXT_CHAR

_SG_USER_INPUT
		PUSH HL
		PUSH AF
		PUSH BC
		CALL BOTTOM_ICO_GRN
		CALL ZX_CL_LOWER
		LD A,">"
		CALL ZX_PRINT
		CALL BOTTOM_ATTR_RESET
		DUP 25
		HALT
		EDUP
		LD A,"_"
		CALL ZX_PRINT
		LD A,#08 ; bksp
		CALL ZX_PRINT
		POP BC
		POP AF
		POP HL
		

		CALL SG_KEYBD_IN

		; Now Z7BUFF has a 00-terminated user input (and trash from previous file unpackings, obviously)
		PUSH HL ; store the current ptr
		LD HL,KENSAKU_LOC ; Set script pos to beginning
_SG_FIND_NEXT_START_OF_PTR
		LD A,(HL)
		CP SCMD_PTRLIST ; FD is start of ptr list
		JP Z,_SGUI_PROC_PLIST
		CP SCMD_EOF ; F7 is EOF
		JP Z,_SGUI_EOF
		INC HL
		JP _SG_FIND_NEXT_START_OF_PTR
_SGUI_PROC_PLIST
		; Compare every string with Z7BUFF
		INC HL ; HL now points at start of string 1
		LD DE,Z7BUFF
		PUSH HL
		;DI:HALT
		CALL StrictStrCmp
		POP HL
		JP Z, _SG_FIND_END_OF_PTR ; If match, scroll until end of ptr block and execute script further
		; otherwise skip until end of string
_SGUI_SCRLL 
		INC HL
		LD A,(HL)
		CP #00 ; nul-terminated string
		JP NZ,_SGUI_SCRLL
		; Check if we are at end of block
		INC HL
		LD A,(HL)
		CP SCMD_PTREND ; FE is end of block
		JP Z,_SG_FIND_NEXT_START_OF_PTR ; if so, scroll until next ptr block
		; otherwise
		DEC HL
		JP _SGUI_PROC_PLIST ; move on with comparison

_SGUI_EOF   PUSH DE
			PUSH BC
			PUSH HL
			ld HL,zeend-1
			ld b,26
_SGUI_BCOM_PRN INC HL
				LD A,(HL)
				PUSH BC
				PUSH HL
				CALL ZX_PRINT
				POP HL
				POP BC
				HALT
				DJNZ _SGUI_BCOM_PRN
			POP HL
			POP BC
			POP DE
			
			POP HL
			DEC HL ; so we hit on input again
			CALL SG_WAIT_KEY
			
			JP _SG_NEXT_CHAR

zeend defb #0D,'Bad command or file name.'

_SG_SET_GAMEFLAG
		INC HL
		LD B,0
		LD C,(HL) ; flag number? 
		INC HL
		LD A,(HL)	; what value?
		PUSH HL
		LD HL, GAMEFLAGS ; pointer to flag area
		ADD HL,BC ; add flag number
		LD (HL), A ; write value
		POP HL ; get back char pointer
		JP _SG_NEXT_CHAR 

_SG_SET_TEMPFLAG
		INC HL
		LD D,(HL) ; get new values from script
		PUSH HL
		LD HL,TEMPFLAGS
		LD A,(HL) ; get current values from memory
		OR D ; add new values to current
		LD (HL),A ; write back to memory
		POP HL ; get back the script pointer
		JP _SG_NEXT_CHAR

_SG_SET_SYSFLAG
		INC HL
		LD D,(HL) ; get new values from script
		PUSH HL
		LD HL,SYSFLAGS
		LD A,(HL) ; get current values from memory
		OR D ; add new values to current
		LD (HL),A ; write back to memory
		CALL SG_SAVE_SFLAGS ; write to disk
		POP HL ; get back the script pointer
		JP _SG_NEXT_CHAR

_SG_CHK_TEMPFLAG
		INC HL
		LD D,(HL) ; get values to compare from script
		PUSH HL
		LD HL,TEMPFLAGS
		LD A,(HL) ; get values from memory
		AND D ; only those that are in both script and memory are kept
		CP D ; are all from script kept?
		POP HL
		JP Z,_TF_MATCH
		; if mismatch, go to else or endif
		CALL SG_ADVANCE_TO_ELSE_OR_ENDIF
_TF_MATCH JP _SG_NEXT_CHAR ; if match, go on

_SG_CHK_SYSFLAG
		INC HL
		LD D,(HL) ; get values to compare from script
		PUSH HL
		LD HL,SYSFLAGS
		LD A,(HL) ; get values from memory
		AND D ; only those that are in both script and memory are kept
		CP D ; are all from script kept?
		POP HL
		JP Z,_SF_MATCH
		; if mismatch, go to else or endif
		CALL SG_ADVANCE_TO_ELSE_OR_ENDIF
_SF_MATCH JP _SG_NEXT_CHAR ; if match, go on

_SG_GET_GAMEFLAG
		INC HL
_GF_COMPARE		LD C,(HL) ; flag number? 
		LD B,0
		INC HL
		LD A,(HL)	; what value?
		PUSH HL
		LD HL, GAMEFLAGS ; pointer to flag area
		ADD HL,BC ; add flag number
		LD D, (HL) ; actual value in D
		POP HL 
		CP D ; actual value in D vs wanted value in A
		JP NZ, _GF_MISMATCH ; no match, exit to else or endif
		INC HL ; match but what do we have next?
		LD A,(HL)
		CP 0 ; is that a 00 means end-of-condition?
		JP NZ, _GF_COMPARE ; if not, it's a flag number, move on
		JP _SG_NEXT_CHAR ; otherwise no mismatches, continue execution...
_GF_MISMATCH
		CALL SG_ADVANCE_TO_ELSE_OR_ENDIF ; mismatch, exit to nearest else or endif
		JP _SG_NEXT_CHAR

_SG_ELSE ; By default, the SG_ADVANCE... procedure will stop at an ELSE, and the subsequent _SG_NEXT_CHAR will go away from it, leaving it unnoticed
		; So if we hit an ELSE, we were executing the code above it, and now need to move on to the endif
		CALL SG_ADVANCE_TO_ELSE_OR_ENDIF
_SG_ENDIF ; By default, the SG_ADVANCE... procedure will stop at an ENDIF too, and the subsequent _SG_NEXT_CHAR will go away from it, leaving it unnoticed
		 ; So if we hit an ENDIF, we were executing the code above it, and don't give a shit anymore
		JP _SG_NEXT_CHAR

SG_ADVANCE_TO_ELSE_OR_ENDIF ; Args: HL = current position in script
		; Let's go to the current condition level's F2 (else) or F3 (endif)
		PUSH BC ; we will need B to be our level counter
		PUSH AF ; we will need A to compare bytes
		LD B, 0 ; Level counter
_advon	INC HL ; take next byte
		LD A,(HL)
		CP SCMD_ENDIF ; is it "endif"?
		JP Z, _cklv ; it is endif, go to check counter
		CP SCMD_ELSE ; is it "else"?
		JP Z, _cklv ; it is else, go to check counter
		CP SCMD_GETFLAG ; is it "if"?
		JP Z, _iffound
		CP SCMD_GETTF ; is it "if_temp"?
		JP Z,_iffound
		CP SCMD_GETSYS ; is it "if_sys"?
		JP Z,_iffound
		JP _advon ; neither of the if's, just go on
_iffound INC B
		JP _advon ; it is not endif or else, goto next byte
_cklv	LD A, B ; check level counter then...
		CP 0
		JP Z, _adved ; it's a top level else or endif, can continue execution
		LD A,(HL) ; it's not top level...
		CP SCMD_ENDIF ; is it endif at least?
		JP NZ, _advon ; it is else, nothing to do
		DEC B ; yes, it is endif, decrement level!
		JP _advon
_adved	POP AF
		POP BC
		RET ; HL now shows the endif/else point from which can continue execution

SG_WAIT_KEY	; Wait for Enter press
	PUSH BC
	PUSH AF
	PUSH HL
	LD A,%00000100 ; Attributes for bottom screen
		LD (#5C48),A
		LD (#5C8D),A
		LD (#5C8F),A
	LD A,#92
	CALL ZX_PRINT
	LD A,%01000111 ; Attributes for bottom screen
		LD (#5C48),A
		LD (#5C8D),A
		LD (#5C8F),A
_waiting_key
	HALT
	LD BC,#BFFE ; Enter,L,K,J,H
	IN A,(C)
	AND %00000001 ; Enter key
	JP NZ,_waiting_key
	POP HL
	POP AF
	POP BC
	RET

BOTTOM_ICO_GRN
	LD A,%00000100 ; Attributes for bottom screen
		LD (#5C48),A
		LD (#5C8D),A
		LD (#5C8F),A
	
	RET

BOTTOM_ICO_RED
	LD A,%00000010 ; Attributes for bottom screen
		LD (#5C48),A
		LD (#5C8D),A
		LD (#5C8F),A
	
	RET

BOTTOM_ATTR_RESET
	LD A,%01000111 ; Attributes for bottom screen
		LD (#5C48),A
		LD (#5C8D),A
		LD (#5C8F),A
		RET

SG_KEYBD_IN
	PUSH HL
	;PUSH BC
	;PUSH AF

		LD HL,Z7BUFF ; Current input address
		LD B,0 ; Will contain count of chars
		XOR A ; Will contain current char
_INLOOP	 ; 32 char limit removed because using ZBuffer
		PUSH BC
		PUSH AF
		; Key Checker
		LD BC, 65278 ; V C X Z CS
		IN A,(C) 
		RRA ; Carry now has CS, don't give a shit
		RRA : JP NC,KEY_Z
		RRA : JP NC,KEY_X
		RRA : JP NC, KEY_C
		RRA : JP NC, KEY_V

		LD BC, 65022 ; G F D S A
		IN A,(C) 
		RRA : JP NC, KEY_A
		RRA : JP NC, KEY_S
		RRA : JP NC, KEY_D
		RRA : JP NC, KEY_F
		RRA : JP NC, KEY_G

		LD BC, 64510 ; TREWQ
		IN A,(C) 
		RRA : JP NC, KEY_Q
		RRA : JP NC, KEY_W
		RRA : JP NC, KEY_E
		RRA : JP NC, KEY_R
		RRA : JP NC, KEY_T

		LD BC, 63486 ; 5 4 3 2 1
		IN A,(C) 
		RRA : JP NC, KEY_1
		RRA : JP NC, KEY_2
		RRA : JP NC, KEY_3
		RRA : JP NC, KEY_4
		RRA : JP NC, KEY_5

		LD BC, 61438 ; 6, 7, 8, 9, 0
		IN A,(C) 
		RRA : JP NC, KEY_0
		RRA : JP NC, KEY_9
		RRA : JP NC, KEY_8
		RRA : JP NC, KEY_7
		RRA : JP NC, KEY_6

		LD BC, 57342 ; Y, U, I, O, P
		IN A,(C) 
		RRA : JP NC, KEY_P
		RRA : JP NC, KEY_O
		RRA : JP NC, KEY_I
		RRA : JP NC, KEY_U
		RRA : JP NC, KEY_Y

		LD BC, 49150 ; H, J, K, L, Enter
		IN A,(C) 
		RRA : JP NC, KEY_ENTER
		RRA : JP NC, KEY_L
		RRA : JP NC, KEY_K
		RRA : JP NC, KEY_J
		RRA : JP NC, KEY_H

		LD BC, 32766 ; B, N, M, Symbol Shift, Space
		IN A,(C) 
		RRA : JP NC, KEY_SPC
		RRA  ; No shit about shift
		RRA : JP NC, KEY_M
		RRA : JP NC, KEY_N
		RRA : JP NC, KEY_B

		POP AF
		POP BC
		HALT
		JP _INLOOP

_OFFLIM ;POP AF
_ENDINPUT	POP AF
	POP BC
	POP HL
	RET

KEY_A LD A, "a" : JP KEY_IN
KEY_B LD A, "b" : JP KEY_IN
KEY_C LD A, "c" : JP KEY_IN
KEY_D LD A, "d" : JP KEY_IN
KEY_E LD A, "e" : JP KEY_IN
KEY_F LD A, "f" : JP KEY_IN
KEY_G LD A, "g" : JP KEY_IN
KEY_H LD A, "h" : JP KEY_IN
KEY_I LD A, "i" : JP KEY_IN
KEY_J LD A, "j" : JP KEY_IN
KEY_K LD A, "k" : JP KEY_IN
KEY_L LD A, "l" : JP KEY_IN
KEY_M LD A, "m" : JP KEY_IN
KEY_N LD A, "n" : JP KEY_IN
KEY_O LD A, "o" : JP KEY_IN
KEY_P LD A, "p" : JP KEY_IN
KEY_Q LD A, "q" : JP KEY_IN
KEY_R LD A, "r" : JP KEY_IN
KEY_S LD A, "s" : JP KEY_IN
KEY_T LD A, "t" : JP KEY_IN
KEY_U LD A, "u" : JP KEY_IN
KEY_V LD A, "v" : JP KEY_IN
KEY_W LD A, "w" : JP KEY_IN
KEY_X LD A, "x" : JP KEY_IN
KEY_Y LD A, "y" : JP KEY_IN
KEY_Z LD A, "z" : JP KEY_IN
KEY_SPC LD A, " " : JP KEY_IN

KEY_1 LD A, "1" : JP KEY_IN
KEY_2 LD A, "2" : JP KEY_IN
KEY_3 LD A, "3" : JP KEY_IN
KEY_4 LD A, "4" : JP KEY_IN
KEY_5 LD A, "5" : JP KEY_IN
KEY_6 LD A, "6" : JP KEY_IN
KEY_7 LD A, "7" : JP KEY_IN
KEY_8 LD A, "8" : JP KEY_IN
KEY_9 LD A, "9" : JP KEY_IN
KEY_0 
		; Backspace handling...
			LD BC, 65278 ; V C X Z CS
			IN A,(C) 
			RRA ; NC is now CS-pressed
			JP NC, KEY_BKSP
		LD A, "0" : JP KEY_IN

KEY_IN LD (HL),A
		PUSH HL
		PUSH AF
		PUSH BC
		CALL ZX_PRINT
		LD A,"_"
		CALL ZX_PRINT
		LD A,#08
		CALL ZX_PRINT
		POP BC
		POP AF
		POP HL
		LD A,16 ; beeper high
		OUT (#FE),A
		DUP 8
		HALT
		EDUP
		XOR A
		OUT (#FE),A
	   POP AF
	   POP BC
	   INC HL
	   JP _INLOOP

KEY_ENTER
		; INC HL
		XOR A
		LD (HL),A ; nul-terminated, the ZBuffer may have leftover thrash
		PUSH HL
		PUSH AF
		PUSH BC
		LD A," " ; hides cursor
		CALL ZX_PRINT
		POP BC
		POP AF
		POP HL
		JP _ENDINPUT

KEY_BKSP
		PUSH AF
		PUSH DE
			LD DE, Z7BUFF ; Check if we are on beginning already
			LD A,H
			CP D
			JP NZ,_bksp_can
			LD A,L
			CP E
			JP NZ,_bksp_can
			; if we reached here, we are at start of string, no allow editing!
			POP DE: POP AF
	   POP AF
	   POP BC
			JP _INLOOP

_bksp_can POP DE
		POP AF
		DEC HL
		LD (HL),#00
		PUSH HL
		PUSH AF
		PUSH BC
		LD A," "
		CALL ZX_PRINT
		LD A,#08
		CALL ZX_PRINT
		LD A,#08
		CALL ZX_PRINT
		LD A,"_"
		CALL ZX_PRINT
		LD A,#08
		CALL ZX_PRINT
		POP BC
		POP AF
		POP HL
		
		LD A,16 ; beeper high
		OUT (#FE),A
		DUP 8
		HALT
		EDUP
		XOR A
		OUT (#FE),A
			   POP AF
	   		POP BC
		JP _INLOOP

SG_TICK_EFFECT
	PUSH AF
	DUP 20
	LD A,16 ; beeper high
	OUT (#FE),A
	XOR A
	OUT (#FE),A
	EDUP
	POP AF
	RET

SG_CLEAR_BOTTOM
		PUSH HL
		PUSH AF
		LD A,%00100001 ; TV Flag: lower screen, need clear
		LD (ZX_TV_FLAG),A
		CALL ZX_CL_LOWER
		LD A,%00000001 ; TV Flag: lower screen, need clear
		LD (ZX_TV_FLAG),A
		POP AF
		POP HL
		RET


SG_TICK defb 0
ZX_PRINT EQU #09F4
ZX_TV_FLAG EQU #5C3C
ZX_CL_LOWER EQU #0D6E

STR_GAME_OVER db "RANDOMIZE USR 25000",00