	device zxspectrum128
;;;;;;;;;;;;Бейсик
 MODULE boot
Basic:
 db #00,10;номер строки
 DW EndLine1 - Line1
Line1:
;randomize usr val "15619":rem:LOAD "SG" CODE 25000
;здесь токены Бейсика
;Бейсик команды запоняются токенами:
 db #20,#FD,#B0 
 db 34,"24999",34
 db #3A; :
 
 db #F9;randomize
 db #C0;usr
 db 176;val
 
 db 34,"15619",34;"15619"
 
 db ":"
 db 234;REM
 db ":"
 
 db 239;load
  db 34,"SG",34; "SG"
 
 db 175;CODE
 db 176;val
 db 34,"25000",34;"25000"
 db #0D;<----------------конец строки
EndLine1:
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;строка 20
 db #00,20;номер строки
 DW EndLine2 - Line2
Line2:
;randomize usr val "15619":rem:LOAD "SG" CODE 25000
;здесь токены Бейсика
 db #F9;randomize
 db #C0;usr
 db 176;val
 
 db 34,"15619",34;"15619"
 
 db ":"
 db 234;REM
 db ":"
 
 db 239;load
  db 34,"VT",34; "VG"
 
 db 175;CODE
 db 176;val
 db 34,"49152",34;"49152"
 db #0D
EndLine2:
;;;;;;;;;;;;;;;;;;;;;;;строка 30
 db #00,30;номер строки
 DW EndLine3 - Line3
Line3:
;RANDOMIZE USR 25000
 db #F9;randomize
 db #C0;usr
 db 176;val
 
 db 34,"25000",34;"25000"
 db #0D
EndLine3:
 
 db #80
 db #AA,1,0;;;;;;;;;;;;;autorun line,change program length to -4, e.g. 83-4=79
EndBasic:
 ENDMODULE
 
    EMPTYTRD "HENIKUUKAN.trd" ;create empty TRD image
    SAVETRD "HENIKUUKAN.trd", "boot.B", boot.Basic, boot.EndBasic - boot.Basic