	DEVICE ZXSPECTRUM128

	; NB! Not supported since 2018/02, version with SGFS makes heavy use of memory paging
	DEFINE USE_PAGES_FOR_MUSIC 1
	
	OUTPUT "sg.bin"
	ORG 25000
	
	INCLUDE "main.asm"


	