[Module]
VortexTrackerII=1
Version=3.6
Title="Village PSG3"
Author=SGPSGConverter
NoteTable=2
ChipFreq=1520640
Speed=4
PlayOrder=L0,1,2,3

[Ornament1]
L0

[Ornament2]
L0

[Ornament3]
L0

[Ornament4]
L0

[Ornament5]
L0

[Ornament6]
L0

[Ornament7]
L0

[Ornament8]
L0

[Ornament9]
L0

[Ornament10]
L0

[Ornament11]
L0

[Ornament12]
L0

[Ornament13]
L0

[Ornament14]
L0

[Ornament15]
L0

[Sample1]
Tne +000_ +00_ F_ L

[Sample2]
Tne +000_ +00_ F_ L

[Sample3]
Tne +000_ +00_ F_ L

[Sample4]
Tne +000_ +00_ F_ L

[Sample5]
Tne +000_ +00_ F_ L

[Sample6]
Tne +000_ +00_ F_ L

[Sample7]
Tne +000_ +00_ F_ L

[Sample8]
Tne +000_ +00_ F_ L

[Sample9]
tne +000_ +00_ 0_ L

[Sample10]
tne +000_ +00_ 0_ L

[Sample11]
tne +000_ +00_ 0_ L

[Sample12]
tne +000_ +00_ 0_ L

[Sample13]
tne +000_ +00_ 0_ L

[Sample14]
tne +000_ +00_ 0_ L

[Sample15]
tne +000_ +00_ 0_ L

[Sample16]
tne +000_ +00_ 0_ L

[Sample17]
tne +000_ +00_ 0_ L

[Sample18]
tne +000_ +00_ 0_ L

[Sample19]
tne +000_ +00_ 0_ L

[Sample20]
tne +000_ +00_ 0_ L

[Sample21]
tne +000_ +00_ 0_ L

[Sample22]
tne +000_ +00_ 0_ L

[Sample23]
tne +000_ +00_ 0_ L

[Sample24]
tne +000_ +00_ 0_ L

[Sample25]
tne +000_ +00_ 0_ L

[Sample26]
tne +000_ +00_ 0_ L

[Sample27]
tne +000_ +00_ 0_ L

[Sample28]
tne +000_ +00_ 0_ L

[Sample29]
tne +000_ +00_ 0_ L

[Sample30]
tne +000_ +00_ 0_ L

[Sample31]
tne +000_ +00_ 0_ L

[Pattern0]
....|..|R-- 1..D ....|F-5 1..D ....|F-3 1..E ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|G-5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|C-5 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|F-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|A#4 .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|C-5 .... ....|A#5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|C-6 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G#5 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|F-5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|--- .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|G-5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|C-5 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|F-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|A#4 .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|C-6 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|R-- .... ....|C-6 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|C-6 .... ....|F-3 .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|F-5 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|D#5 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|--- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....

[Pattern1]
....|..|R-- .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|G-5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|C-5 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|F-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|A#4 .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|C-5 .... ....|A#5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|C-6 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G#5 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|F-5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|--- .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|G-5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|C-5 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|R-- .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|F-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G-5 .... ....|G-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|A#4 .... ....|D#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|R-- .... ....|R-- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|C-4 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|C-6 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|R-- .... ....|R-- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....
....|..|G-4 .... ....|A#5 .... ....|G#4 .... ....
....|..|--- .... ....|--- .... ....|--- ...F ....
....|..|G#4 .... ....|C-6 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-4 .... ....|G#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|C-6 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|F-5 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-4 .... ....|D#5 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|--- .... ....|--- .... ....
....|..|--- ...F ....|--- .... ....|--- .... ....

[Pattern2]
....|..|A#4 .... ....|A#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G#5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|A#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|C-6 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G-5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|--- .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#5 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|--- .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|--- .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|G#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|A#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|A#5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|G#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G#5 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|F-5 .... ....|D#4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|D#5 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-4 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#4 .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-4 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#4 .... ....|G#5 .... ....|C#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-4 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|--- .... ....|C#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|--- .... ....|G#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|--- .... ....|G#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-4 .... ....|--- .... ....|G#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....

[Pattern3]
....|..|A#4 .... ....|A#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|G-5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G#5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#4 .... ....|A#5 .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C-5 .... ....|C-6 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|G-5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|--- .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#5 .... ....|F-5 .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D#5 .... ....|--- .... ....|F-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|D#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|--- .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|G#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|A#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|A#5 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|C-6 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G#5 .... ....|C#6 .... ....|G-2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|C#6 .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|D#6 .... ....|G-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|C#6 .... ....|G#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|--- .... ....|G#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|A#4 .... ....|A#5 .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|C#5 .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|C-6 .... ....|C-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|C-4 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|A#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|R-- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|G-5 .... ....|C#6 .... ....|A#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|C-6 .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|F-5 .... ....|A#5 .... ....|G#2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|E-5 .... ....|G#5 .... ....|G#3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|D-5 .... ....|--- .... ....|R-- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|E-5 .... ....|G-5 .... ....|G-2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|G-3 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|E-5 .... ....|G-2 .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....
....|..|--- .... ....|--- .... ....|--- .... ....

