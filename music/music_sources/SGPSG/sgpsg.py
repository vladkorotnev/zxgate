#!/bin/env python

## SG-PSG to Vortex converter by Akasaka
## v1.0 2017/06/23

## Changelog
### 1.0 finally this shit works

import pdb, math, sys

# Prompt for file name
inf = sys.argv[1]#raw_input("In? ")#'mus/bgm14_psg.mus' #'bgm14.mus'#'test.mus'#
title = inf

# Read all lines
lines = [line.rstrip('\n') for line in open(inf)]

# Array for MML by-channel
chans_mml = {}

# Option to invert octave command direction
invert_octave = ('--invert-octave' in sys.argv)

# mml command categories filters
type_command = ['>','<','o','[',']','/','l','v','q','(']
type_value = ['1','2','3','4','5','6','7','8','9','0']
type_note = ['c','d','e','f','g','a','b','r','^']
type_mod = ['+','-','#']

# note list for "-" command
notes = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B']

# preprocess by sorting into channel array
for line in lines:
	if len(line) < 1:
		continue
	if line[:2] == "//" or line[0] == "#":
		if line[:(len('#title'))] == '#title':
			title = line[(len('#title')):].strip()
		elif line[:(len('#octave'))] == '#octave':
			invert_octave = True
		continue
	if line[0] == "@":
		# def envelope/var - todo
		continue
	data = line.split('	')
	for chan in data[0]:
		if not chan in chans_mml:
			chans_mml[chan] = []
		chans_mml[chan].append( data[1] )
		
# now chans mml has all channel lines split
# time to convert it into vtx

chans_vtx = {}
# each channel contains array of string like "D#3 1..1 ...."

LINES_PER_WHOLE = 32

# alloc channel arrays and fill with data
for cname in chans_mml:
	cchan = [] # current channel arry
	
	volume = 15 # current volume
	newvolume = volume
	step = LINES_PER_WHOLE # lines per note
	octave = 2 # current octave
	note = "C-" # current note

	repeat_counters = [] # Counter stack for repeat commands
	repeat_line_pointers = [] # Line pointer stack for repeat commands
	repeat_char_pointers = [] # Char pointer stack for repeat commands
	rend_line_pointers = [] # End of repeat line pointer stack for repeat commands
	rend_char_pointers = [] # End of repeat char pointer stack for repeat commands
	lidx = 0 # Current line index
	
	# With all lines in MML channel
	while lidx < len(chans_mml[cname]):
		# Get line
		line = chans_mml[cname][lidx]
		# Start from first char
		chidx = 0
		# With all chars...
		while chidx < len(line):
			char = line[chidx]
			if char == ' ': # Ignore spaces
				chidx += 1
				continue
			elif char in type_command:
				# Process octave commands limiting 1..8 (VortexTracker/ZX Protracker/SoundTracker)
				if (char == '>' and not invert_octave) or (char == '<' and invert_octave):
					if octave < 8:
						octave += 1
				elif (char == '<' and not invert_octave) or (char == '>' and invert_octave):
					if octave > 1:
						octave -= 1
				# Octave set command
				elif char == 'o':
					chidx += 1
					octave = int(line[chidx])
				# Repeat Begin command
				elif char == '[':
					# Push current position to stack
					repeat_line_pointers.append(lidx)
					repeat_char_pointers.append(chidx+1)
					# Analyze context to find end of the repeat block
					opens = 1
					lals = line
					lend = lidx
					cend = chidx
					achar = char
					while opens > 0:
						cend += 1
						if cend == len(lals):
							lend += 1
							lals = chans_mml[cname][lend]
							cend = 0
						achar = lals[cend]
						if achar == '[':
							opens += 1
						elif achar == ']':
							opens -= 1
					# Push end position to stack
					rend_line_pointers.append(lend)
					rend_char_pointers.append(cend-1)
					print "START ",lidx,":",chidx+1,"~",lend,":",cend,repeat_counters
					# I know I could parse the count already but idc :p
					repeat_counters.append(-1) # -1 means not yet known
				# Repeat End command
				elif char == ']':
					if repeat_counters[-1] == -1: # if we don't know how much we need more
						# check if we can know the amount needed to repeat more (multi digit search):
						if chidx < len(line)-1:
							if line[chidx+1] in type_value:
								temp = ''
								chidx += 1
								while chidx < len(line) and line[chidx] in type_value:
									temp += line[chidx]
									chidx += 1
								if chidx < len(line) and not (line[chidx] in type_value):
									# if break was because param end
									chidx -= 1
								repeat_counters[-1] = int(temp)
						# If we couldn't find shit
						if repeat_counters[-1] == -1: # nope didn't work lel
							repeat_counters[-1] = 2 # assume 2 times repeated
					# Decrement repeat counter for current level
					repeat_counters[-1] -= 1
					# No more to repeat?
					if repeat_counters[-1] == 0:
						print "END ",repeat_line_pointers[-1],":",repeat_char_pointers[-1],"~",lidx,":",chidx,repeat_counters
						# Remove top stack item (data of current level)
						repeat_counters.pop()
						repeat_line_pointers.pop()
						repeat_char_pointers.pop()
						rend_line_pointers.pop()
						rend_char_pointers.pop()
					else:
						# Have more to repeat? Take coords out of stack
						lidx = repeat_line_pointers[-1]
						chidx = repeat_char_pointers[-1]
						# Get back the line the loop begins on
						line = chans_mml[cname][lidx]
						# Go ahead looping there
						continue
				# Repeat barring command (i.e. [cafe / feca]3 leads to playing {cafe feca cafe feca cafe}
				elif char == '/':
					# if repeating not the last time
					if repeat_counters[-1] > 1 or repeat_counters[-1] == -1: 
						chidx += 1 # just ignore the symbol
						continue
					else:
						# repeating last time, must proceed until end, take items off stack and go back
						lidx = rend_line_pointers[-1]
						chidx = rend_char_pointers[-1]
						line = chans_mml[cname][lidx]
						continue
				# Note Length Set command
				elif char == 'l':
					lgth = 1 # assume whole note, read multi-digit value
					if chidx < len(line)-1:
						if line[chidx+1] in type_value:
							temp = ''
							chidx += 1
							while chidx < len(line) and line[chidx] in type_value:
								temp += line[chidx]
								chidx += 1
							if chidx < len(line) and not (line[chidx] in type_value):
								# if break was because param end
								chidx -= 1
							lgth = int(temp)
					# step is lines per whole line divided by measure
					step = LINES_PER_WHOLE/lgth
				# Volume Set Command
				elif char == 'v':
					# Assume default is max
					newvol = 15
					# Read multi digit
					if chidx < len(line)-1:
						if line[chidx+1] in type_value:
							temp = ''
							chidx += 1
							while chidx < len(line) and line[chidx] in type_value:
								temp += line[chidx]
								chidx += 1
							if chidx < len(line) and not (line[chidx] in type_value):
								# if break was because param end
								chidx -= 1
							newvol = int(temp)
					# Clamp to 1..14
					if newvol > 15: 
						newvol = 15
					if newvol < 1:
						newvol = 1
					newvolume = newvol
					# If we have a line above, edit it to set volume, because further notes already need it
					#if len(cchan)>0:
					#	line_to_edit = cchan.pop()
					#	line_data = line_to_edit.split(' ')
					#	line_data[1] = '...{0:x}'.format(newvol).upper()
					#	cchan.append(' '.join(line_data))
				# Relative volume (1..9)
				elif char == 'r':
					newvol = volume
					if chidx < len(line)-1:
						if line[chidx+1] in type_value:
							temp = ''
							chidx += 1
							while chidx < len(line) and line[chidx] in type_value:
								temp += line[chidx]
								chidx += 1
							if chidx < len(line) and not (line[chidx] in type_value):
								# if break was because param end
								chidx -= 1
							newvol = int(temp)
					if newvol > 9: 
						newvol = 9
					# Vol = RVol/9 * Vol
					newvolume = volume * math.floor(newvol/9)
					# Clamp 1..15
					if newvolume < 1:
						newvolume = 1
					if newvolume > 15:
						newvolume = 15
					# Update line above if we have any
					#if len(cchan)>0:
					#	line_to_edit = cchan.pop()
					#	line_data = line_to_edit.split(' ')
					#	line_data[1] = '...{0:x}'.format(newvol).upper()
					#	cchan.append(' '.join(line_data))
				# Vol Up Command
				elif char == '(':
					# Same as above
					newvol = volume+1
					if newvol > 15: 
						newvol = 15
					if newvol < 1:
						newvol = 1
					newvolume = newvol
					#if len(cchan)>0:
					#	line_to_edit = cchan.pop()
					#	line_data = line_to_edit.split(' ')
					#	line_data[1] = '...{0:x}'.format(newvol).upper()
					#	cchan.append(' '.join(line_data))
				# Vol Down Command
				elif char == ')':
					# Same as above
					newvol = volume-1
					if newvol > 15: 
						newvol = 15
					if newvol < 1:
						newvol = 1
					newvolume = newvol
					#if len(cchan)>0:
					#	line_to_edit = cchan.pop()
					#	line_data = line_to_edit.split(' ')
					#	line_data[1] = '...{0:x}'.format(newvol).upper()
					#	cchan.append(' '.join(line_data))
			# Note Processing!
			elif char in type_note:
				note = char.upper()
				original_step = step # backup in case note changes step
				original_octave = octave # same for octave
				lgth = 1 # init length var in case we have a measure in front of the note
				if chidx < len(line)-1:
					# If we have a sharp
					if line[chidx+1] == '#' or line[chidx+1] == '+':
						note += "#" # Make the note sharp
						chidx+=1 # Move ptr
					# If we have a flat
					elif line[chidx+1] == '-':
						# Transpose 1 semi down
						noteidx = notes.index(note) - 1
						if noteidx == -1: # If we ran below C, 
							noteidx = 11 # Set to B
							if octave > 1: # but of previous octave, if available
								octave -= 1
						# New note received
						note = notes[noteidx]
						# Move ptr
						chidx+=1
					# If we have a length for the specific note
					if chidx < len(line)-1 and line[chidx+1] in type_value:
						# Read multi digit
						lgth = 1
						temp = ''
						chidx += 1
						while chidx < len(line) and line[chidx] in type_value:
							temp += line[chidx]
							chidx += 1
						if chidx < len(line) and not (line[chidx] in type_value):
							# if break was because param end
							chidx -= 1
						lgth = int(temp)
						# Set stepping by length in front of note
						step = LINES_PER_WHOLE/lgth
					# If note is (multi-)dotted
					if chidx < len(line)-1 and line[chidx+1] == '.':
						# Count dots
						dots = 1
						chidx += 1
						while chidx < len(line) and line[chidx]  == '.':
							dots += 1.0
							chidx += 1
						if chidx < len(line) and not (line[chidx]  == '.'):
							# if break was because param end
							chidx -= 1
						# Every dot makes note longer by 1/2 of prev. length
						# I.e. 1/8 dot = 1/8 + 1/16, 1/8 dot dot = 1/8 + 1/32 and so on
						dlen = int(math.ceil(step * (1.0-(1.0/dots))))
						print "dot extend",dlen,"from",step
						step += dlen
						print "now ",step
					# THE MISSING SHIT! It was breaking timing on AkRScript and everything else:
					# THE SYNCOPE
					if chidx < len(line)-1 and line[chidx+1] == '_':
						chidx += 1
						# underscore means the next note should be syncopated, so let's remove the needed amount of lines from what we already have, before pushing in the current note
						for i in range(0,step):
							cchan.pop()
				
				if note == "R":
					note += "--" # Rest sign cannot have an octave
				elif note == '^':
					note = "---" # ^ means just add length, e.g. a8^8 effectively becomes a4 and so on
				else:
					# Any other notes need an octave
					if len(note) == 1:
						note += "-"
					note += str(octave)
				# Add volume data if this is the first line, otherwise empty parameters
				# TODO: Implement envelope/slide/link/etc
				if len(cchan) == 0 or newvolume != volume:
					volume = newvolume
					cchan.append(note+" ...{0:x} ....".format(volume).upper())
				else:
					cchan.append(note+" .... ....")
				# Pad the channel with empty lines for proper timing
				for i in range(0,step-1):
					cchan.append("--- .... ....")
				# Restore step and octave
				step = original_step
				octave = original_octave
			else:
				# Not sure what instruction that is...
				if not char.isdigit():
					print "unsupported/unexpected:[",char,"] at ",cname,":",lidx,":",chidx
				pass
			chidx+=1 # Next char
		lidx += 1 # Next line
	# Save channel roll...
	chans_vtx[cname] = cchan
	
# normalize channel length to the longest channel
maxl = 0
for cname in chans_vtx:
	if len(chans_vtx[cname]) > maxl:
		maxl = len(chans_vtx[cname])
for cname in chans_vtx:
	if len(chans_vtx[cname]) < maxl:
		for i in range(0,maxl - len(chans_vtx[cname])):
			chans_vtx[cname].append("--- .... ....")

# Max Pattern Length
maxpatlen = 128

# VTX II File Ornament Section blob
out = "[Ornament1]\r\nL0\r\n\r\n[Ornament2]\r\nL0\r\n\r\n[Ornament3]\r\nL0\r\n\r\n[Ornament4]\r\nL0\r\n\r\n[Ornament5]\r\nL0\r\n\r\n[Ornament6]\r\nL0\r\n\r\n[Ornament7]\r\nL0\r\n\r\n[Ornament8]\r\nL0\r\n\r\n[Ornament9]\r\nL0\r\n\r\n[Ornament10]\r\nL0\r\n\r\n[Ornament11]\r\nL0\r\n\r\n[Ornament12]\r\nL0\r\n\r\n[Ornament13]\r\nL0\r\n\r\n[Ornament14]\r\nL0\r\n\r\n[Ornament15]\r\nL0\r\n\r\n"

# VTX II File Sample Section Blob
out += "[Sample1]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample2]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample3]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample4]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample5]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample6]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample7]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample8]\r\nTne +000_ +00_ F_ L\r\n\r\n[Sample9]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample10]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample11]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample12]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample13]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample14]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample15]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample16]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample17]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample18]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample19]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample20]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample21]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample22]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample23]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample24]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample25]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample26]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample27]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample28]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample29]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample30]\r\ntne +000_ +00_ 0_ L\r\n\r\n[Sample31]\r\ntne +000_ +00_ 0_ L\r\n\r\n"

# Begin forming the pattern section blob (VTX II format)
out += "[Pattern0]\r\n"
# Current pattern and it's length
curpat = 0
curpatlen = 0
# With all lines
for i in range(0,maxl):
	if curpatlen == maxpatlen: # If current pattern is full
		curpatlen = 0 # reset counter for lines
		curpat += 1 # increment counter for patterns
		out += "[Pattern"+str(curpat)+"]\r\n" # write new pattern header
	cl = "....|..|" # write empty env/noise tracks (TODO: implement noise track)
	for cn in chans_vtx: # with every channel
		cl += chans_vtx[cn][i] # append the channel data to line
		# if not last channel, add separator...
		if chans_vtx.keys().index(cn) < len(chans_vtx.keys())-1:
			cl += "|"
	# Newline.
	out += cl+"\r\n"
	curpatlen += 1 # Increment counter for lines

# Generate the VTX II module header
head = "[Module]\r\nVortexTrackerII=1\r\nVersion=3.6\r\nTitle="+title+"\r\nAuthor=SGPSGConverter\r\nNoteTable=2\r\nSpeed=2\r\nPlayOrder=L"
# ...with the pattern order
for i in range(0,curpat):
	head += str(i)+","
head += str(curpat)+"\r\n"

# Output file is ready
out = head+out

# Output file path
tgt = sys.argv[2]#raw_input("Out? ")

# Write file
tf = open(tgt, 'w')
tf.write(out)
tf.close()

# Bigtime.
print "Done. ",curpat, " patterns "