CONST_TRDOS_MAX_FILE_SIZE = 0xFF00
CONST_TRDOS_SECTOR_SIZE = 256
CONST_TRDOS_SECTORS_PER_TRK = 16

def filenameToTRDOS(fname, ext='C'):
	if len(fname) > 8:
		print "FATAL:	Filename", fname," longer than 8 char"
	if len(ext) > 1:
		print "FATAL:   Fileext",ext," longer than 1 char"
	fn = "        "+ext
	return fname.upper()+fn[len(fname)::]