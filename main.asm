
; ----------- ENTRY POINT ---------------
	IFDEF USE_PAGES_FOR_MUSIC
		DI
		ld a,$11
		ld bc,$7ffd
		OUT (C), A
		EI
	ENDIF

	CALL CLR_ATTR
	CALL CUSTOM_CHARS
	CALL SGFS_INIT_LOOKUP_TABLE
	CALL SG_READ_SYSFLAGS
	JP SG_RUN_FROM_START

; ----------- System Character Font ---------------
	INCLUDE "engine/font.asm"

; ----------- System ---------------

; Note the memory bank usage!
; Bank 0 is used for BGM (code + soundtrack)
; Bank 1 is used for script, contiguous with bank2, used for system, and is active most of the time
; Bank 2 is used for the system, and for the beginning of the script, contiguous with bank2
; Bank 3 is used for the volume lookup table

	INCLUDE "engine/disk.asm"
	INCLUDE "engine/bgm.asm"
	INCLUDE "engine/screen.asm"
	INCLUDE "engine/reset.asm"
	INCLUDE "engine/fadeout.asm"
	INCLUDE "engine/commandset.asm"
	INCLUDE "engine/script.asm"
	INCLUDE "engine/cpstr.asm"


; ----------- Sysvars ---------------
STRT_SCPT	db "SGBOOT  C",#00
SCPT_OFFS	dw 0
BGM_ON 		db 0
BGM_NAME	db "NOBGM   C",#00

START_GAMEDATA ; For Save -- fit in 1 sector

SCPT_NAME	db "SGBOOT  C",#00
GRPH_NAME   db "NOGRAPH C",#00

TEMPFLAGS defb #00 ; Reset every scene change

GAMEFLAGS DUP 211  ; Persistent on player, count: to fit a SAVEFILE into 1 sector of floppy!
			defb #00
		EDUP
END_GAMEDATA ; For Save

SYSFLAGS  defb #00 ; Persistent on system



; ----------- Decompressor buffer ------------------

Z7BUFF DUP 5000
		db #00
	EDUP

; --------------------------------------------------



KENSAKU_LOC
		; System Command Script
		defb SCMD_PTRLIST, "ver",0, SCMD_PTREND
		defb "ZXGate Engine v1.2.1 with SGFS.",0
		defb "Engine by Genjitsu Labs 2017.",0
		defb "SGFS by Genjitsu Labs 2018.",0
		defb SCMD_INPUT
		; Include Extra commands by the game author
		INCBIN "SYSTEM_SCRIPT.C"

SCPT_LOC 
	; Place MAIN SCRIPT here
		INCBIN "MAIN_SCRIPT.C"

