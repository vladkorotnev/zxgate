#!/usr/local/bin/fish


# Clean stage
rm -rf out/*

# Precompile system script
python script2zx.py --no-eof --pad-lines system_script.txt
python script2zx.py --no-eof --pad-lines main_script.txt

# Assemble engine
sjasmplus build_me.asm 
mv sg.bin out/SG.C

# Remove system script
rm SYSTEM_SCRIPT.C
rm MAIN_SCRIPT.C

# Assemble boot
#sjasmplus boot.asm

# Move Boot to Stage
mctrd new HENIKUUKAN.trd
mv HENIKUUKAN.trd out/

mkdir out/bin
cp bin/* out/bin/


echo "Building data..."
# Prepare compressed files of everything

# Compress music
mkdir out/music
cd music
mkdir compressed
for i in *.C
	echo "Compressing: " $i
	zx7 "$i" "compressed/$i" | grep from
end
mv compressed/* ../out/music/
rm -rf compressed
cd ..


# Compress graphics
mkdir out/graphics
cd graphics
mkdir compressed
for i in *.scr
	echo "Compressing: " $i
	set fn (basename $i .scr | tr '[a-z]' '[A-Z]')
	zx7 "$i" "compressed/$fn.C" | grep from
end
mv compressed/* ../out/graphics/
rm -rf compressed
cd ..



echo "Building game script..."
# Compile all script files
mkdir out/script
cd script
python ../script2zx.py --pad-lines *.txt
mv *.C ../out/script/
cd ..

cd out
python ../sgfs.py GAMEDAT ./music/*.C ./graphics/*.C ./script/*.C

# add SGFS images to TR-DOS image
for i in GAMEDAT*.C
	mctrd add $i HENIKUUKAN.trd
end

# add all binaries to TR-DOS image
for i in bin/*
      mctrd add $i HENIKUUKAN.trd
end

# add MAIN to TR-DOS image
mctrd add SG.C HENIKUUKAN.trd




mkdir disk
mv *.trd disk/

open disk/HENIKUUKAN.trd
