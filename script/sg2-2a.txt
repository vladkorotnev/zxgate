

@play file=bgm02

@ifflag name="moeka_asleep" value="0" name="upa_get" value="0"
	@draw file=cg17_a
@endif

@ifflag name="moeka_asleep" value="1" name="upa_get" value="0"
	@draw file=cg17_c
@endif

@ifflag name="upa_get" value="1"
	@draw file=cg17_b
@endif

@ifflag name="flag2_2a_01 value="0"
@flag name="flag2_2a_01" value="1"

	Since the elevator's stopped, you end up ascending the dim staircase all the way to the 7th floor.
	[Rintarou] "Hahh, whew, fwewh, hahh, pwah..."
	You feel like dying once you reach the 7th floor. Which, by the way, is the only illuminated floor.
	Furthermore, you can feel someone's presence. Someone's at the 7th floor landing.
	An Organization spy!? No- Neithardt!?
	On further inspection, it's a familiar face.
	[Moeka] "..."
	Kiryuu Moeka. AKA Shining Finger. Why is she here...?
	Moeka is Lab Mem Number 005. Currently works part-time at the Braun Tube Workshop downstairs from the lab.
	And, in other world lines, your mutual enemy.
	Why would Moeka be here, In the closed Radio Kaikan? Your suspicious mind has a few ideas--
	That in this world line, she's involved with SERN and the Rounders once again.
	That this Neithardt, the man who obtained the IBN 5100, could also be a rounder.
	In that case, it'll be bad if Moeka finds you here.
	Besides, you have yet to check Moeka's status as a Rounder in this world line.
	Dammit. So close. The capsule toy machine's right there....
@else
	@ifflag name="upa_get" value="0"
		You peek at the seventh floor. Looks like Moeka's still sitting on the staircase with her eyes closed.
	@else
		Silence has returned to the seventh floor. Looks like Moeka's... gone.
	@endif
@endif
	@input



*look
*look around
	Just ahead is the device where you can test your luck with an exchange of 100 yen and a twist of a lever.
	It's got a real name, but it's awkard when adults say it.
	To the left is a staircase that leads both up and down.
	@input

;//――――――――――――――――――――――――――――――――――――――――

*f
*front
*forward

@ifflag name="moeka_asleep" value="0" name="upa_get" value="0"
	You cannot pass unless you put Moeka to sleep.
	What was the spell to put Moekas to sleep, again? Uhh, the first part was "el psy", and, uh... I forget the rest.
	@input
@endif

@ifflag name="moeka_asleep" value="1" name="upa_get" value="0"
	@stop
	@clear
	@goto file=SG2-3a
@endif

@ifflag name="upa_get" value="1"
	You don't need anything from the capsule toy corner.
@endif
	@input

*d
*down
*b
*back
	@stop
	@clear
	@goto file=SG2-1a


*u
*up
*l
*left
@ifflag name="moeka_asleep" value="0" 
	As long as Potential Rounder Moeka is present, you cannot go to the roof.
	You need to do something about her.
	@input
@else
	@stop
	@clear
	@goto file=SG2-4a
@endif

*look wall
*r
*right
	That's a pretty cold looking wall.
	@input

*look poster
	You can't really tell what sort of poster that is from here.
	@input

*look fire extinguisher
*look fireextinguisher
*look shokaki
*look fire
*look extinguisher
	That's it! You could use that fire extinguisher, and....
	Wait, no! No you can't!
	@input

*congroo
*kongroo
*congloo
*kongloo
*shout
@ifflag name="upa_get" value="0"
	@stop

	[Rintarou] "El Psy Congroo"
	[Moeka] "Okabe-kun...?"
	
	@play file=bgm07
	@draw file=cg17_d
	
	Crap! It had the reverse effect!
	[Moeka] "This area's off limits. Please leave."
	[Rintarou] "Are you a Rounder agent?"
	[Moeka] "A what? No, I'm an Interpol agent."
	[Rintarou] "...Was that some sort of joke?"
	[Moeka] "Can't believe I got found out during an undercover investigation.... Sorry, but I have to restrain you until the investigation's over."
	[Rintarou] "S-stop joking around! Like hell Interpol actually exists!"
	At that moment, you feel a presence behind you.
	You turn around, and see several unfamiliar foreigners in black suits standing there.
	To add to your confusion, they suddenly push you down.
	As you lay face down, they fasten your wrists together with handcuffs.
	[Rintarou] "Hey, this is an unjust arrest! No, wait, eh, seriously? Interpol? Who the hell do you think you are, Inspector Zenigata!?"
	[Moeka] "Quiet. Here."
	Moeka holds something out to your face. Looks like a legit Interpol badge.
	@stop
	@gameover
@else
	There's no point in talking to yourself.
@endif
	@input

*talk
*talk moeka

@ifflag name="moeka_asleep" value="0" name="upa_get" value="0"
	You can't! Did you already forget what you came here for!?
	This is an infiltration mission.
	You're not supposed to let -anyone- notice you.
	@input
@endif

@ifflag name="moeka_asleep" value="1" name="upa_get" value="0"
	Moeka's nodding off.
	@input
@endif

@ifflag name="upa_get" value="1"
	Moeka's nowhere to be found.
@endif
	@input



*look moeka
@ifflag name="moeka_asleep" value="0" name="upa_get" value="0"
	Moeka's gazing at her phone as she lounges on the staircase.
	She's muttering something, but you can't hear it from here.
	On closer inspection, her body's swaying. Occasionally, her head falls to the side.
	Is she sleepy?
	@input
@endif

@ifflag name="moeka_asleep" value="1" name="upa_get" value="0"
	Moeka's nodding off.
	@input
@endif

@ifflag name="upa_get" value="1"
	Moeka's nowhere to be found.
@endif
	@input


*hear
*listen
@ifflag name="moeka_asleep" value="0" name="flag2_2a_hear" value="0"
	@flag name="flag2_2a_hear" value="1"
	You listen carefully. You barely manage to hear something:
	[Moeka] "IBN 5100... somewhere... here... in Akiba... find... find it..."
	@input
@endif

@ifflag name="moeka_asleep" value="0" name="flag2_2a_hear" value="1"
	[Moeka] "Badge inscription... raise... flag..."
	In Japanese, plz. What does she mean by badge?
	Now that it's come to this, maybe you should try being a little more aggressive...?
@else
	You listen carefully, but you can't hear anything.
@endif
	@input


*wait
@ifflag name="moeka_asleep" value="0" name="flag2_2a_wait" value="0"
@flag name="flag2_2a_wait" value="1"
	Moeka's eyes look very sleepy.
	@input
@endif


@ifflag name="moeka_asleep" value="0" name="flag2_2a_wait" value="1"
@flag name="flag2_2a_wait" value="2"
	Moeka looks extremely sleepy.
	@input
@endif

@ifflag name="moeka_asleep" value="0" name="flag2_2a_wait" value="2"
@flag name="flag2_2a_wait" value="3"
	Moeka doesn't look sleepy. She looks asleep.
	@input
@endif


@ifflag name="moeka_asleep" value="0" name="flag2_2a_wait" value="3"
@flag name="flag2_2a_wait" value="4"
	Moeka's definitely asleep. Amazing how she's still gripping her phone.
	@flag name="moeka_asleep" value="1"
	@draw file=cg17_c
	@input
@endif

@ifflag name="moeka_asleep" value="1" name="upa_get" value="0"
	Moeka's sound asleep.
	@input
@else
	You wait a while, but nothing happens.
@endif
	@input


*phone
*assistant
*call assistant
@ifflag name="upa_get" value="0"
	Use your phone here, and Moeka will catch you.
@else
	Hm? That's strange. I'm not getting a signal.
@endif
	@input


*d mail
*time leap
	You can't do that in this world line.
	@input


*attack
*attack moeka
*kick
*kick moeka
*punch
*punch moeka
*kill
*kill moeka
	Quit it. You've already forgiven her.
	@input

*kiss
*kiss moeka
*chu chu
*chu
*chu moeka
	Do that, and you'll wake her up.
	@input

*get moeka
*take moeka
*use moeka
	No, no, no. Wait a sec.
	What are you going to do with this mute, useless woman?
	I mean, you still don't know if she's a Rounder in this world line.
	Well, anyway, you don't need her.
	@input


*hug
*hug moeka
	You should avoid any actions that could wake up Shining Finger.
	@input



*get shoukaki
*get fire extinguisher
*take shoukaki
*take fire extinguisher
*use shoukaki
*use fire extinguisher
*get extinguisher
*take extinguisher
*use extinguisher
	You see two fire extinguishers. But you don't have any use for them, do you?
	@input


