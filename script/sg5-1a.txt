@play file=bgm02

@ifflag name="flag5_1a_01" value="0"
	@flag name="flag5_1a_01" value="1"
	@draw file=cg35_a
	Haah. Back at the lab. Finally.
	You took so many detours, but didn't get much results. So now you feel worn out.
	You're here at the Oohiyama building, on the outskirts of Akihabara.
	Home to the Future Gadget Laboratory on the second floor.
	By the way, the first floor is the Braun Tube Workshop. A CRT specialty store.
	Remember that Tennouji guy from the station? He's the owner of this building.
	@draw file=cg57
	The Tennouji's were at the station, so of course, the workshop door has a "be back soon" sign on it.
	@draw file=cg35_b
	[Mayuri] "Ah, it's Okarin! Tutturu-"
	Loitering in front of the building is Lab Mem Number 002, Shiina Mayuri.
	But for some reason, she doesn't look too happy.
	[Rintarou] "What's wrong?"
	[Mayuri] "Umm, well, it looks like I dropped Granny's Pocketty~..."
	[Mayuri] "I've been looking for it, but I couldn't find it. Okarin, have you seen it-?"
	Pocketty~ is an old pocket watch.
	It's a memento of her late grandmother. She always carries it with her.
	[Mayuri] "Pocketty~san, Pocketty~san, come out, come out, wherever you are-"
	@flag name="pockettyyyy" value="1"
	@input
@endif

@ifflag name="flag5_1a_01" value="1" name="osxxkufx2010" value="0"
	@draw file=cg35_b
	Mayuri's still looking for Pockety~.
	[Mayuri] "Pocketty~ Pocketty~ Pockety~sa~n?"
	Is she calling for it, or serenading it? In any case, Mayuri's got a field of flowers for a brain.
	@input
@endif

@ifflag name="flag5_1a_01" value="1" name="osxxkufx2010" value="1"
	@draw file=cg35_c
	[Mayuri] "Ah, Okari-n. Tutturu-♪"
	Mayuri's sitting on the bench in front of the Braun Tube Workshop, eating a banana.
	In her hand, she's got Pockety~.
	[Mayuri] "Mayushii's eating snacks outside. The breeze is so nice~"
	[Rintarou] "You're just like a kindergartender. Just one look at you, and I can forget about all my worries about this critical situation..."
	[Mayuri] "Clinical carbonation~?
	[Rintarou] "No, it's nothing. Nothing at all. Nothing to worry about."
	[Rintarou] "You just have to keep smiling for me."
	[Mayuri] "I don't really get it, but I just need to smile, right? Ehheheh-"
	@input
@endif
	@input


*l
*left
	@stop
	@clear
	@goto file=SG4-2a


*b
*back
	Your instincts whisper to you. You must not enter the path between the lab and the multi-tenant buildings.
	You feel a disturbance in your arm.
	Wander around carelessly, and you'll never return. That's right. This is block 1 of hell.
	@input


*r
*right
	You're about to head out to Kuramaebashi Dori, but remember you don't have any real business there.
	@input

*u
*up
*f
*front
*forward
;＞▼5-2　ラボドア前
	@stop
	@clear
	@goto file=SG5-2a


*look
*look mayuri
@ifflag name="osxxkufx2010" value="1"
	Mayuri looks content as she holds her pocket watch.
	Don't lose it again.
	@input
@else
	Mayuri looks less energetic than usual.
	Probably because she lost her favorite pocket watch.
@endif
	@input



*talk
*talk mayuri
@ifflag name="osxxkufx2010" value="1"
	[Mayuri] "Tutturu-♪"
	Yup. Just as she should be.
	@input
@else
	[Rintarou] "Where'd you lose your watch?"
	[Mayuri] "Pocketty~san? Um... I had him until I got to the side street, at least...."
	Hmm, so it -is- there, huh.
@endif
	@input
;//―――――――――――――――――――――――――――――――


*to true
*tuturu
*tutturu
*tutturuu
*toodleoo
*too true
*true dat
	[Rintarou] "Mayuri, tutturu-"
	[Mayuri] "Yup, tutturu-♪"
	@input


*banana
*give me banana
*take banana
	[Mayuri] "Eh? You wanna banana~? I'll give you one-. Welp, here you go♪"
	Mayuri takes a banana out of her bag. Wait, she carries them...?
	You take it and eat it on the spot. Mayuri soon follows.
	A man and a woman eating bananas on the side of the road.... Such a surreal scene.
	[Mayuri] "Delicious~♪"
	She looks so happy just eating a banana. Mayuri seems like such a cheap woman, if you know what I mean. Heheheh.
	@input


*odencan
*give me odencan
*canned oden
*give me canned oden
	[Mayuri] "Ehh~? Okarin, how did you know Mayushii bought canned oden today~?"
	[Rintarou] "I can see through your entire behavioral pattern. Fuhahahah!"
	[Mayuri] "You know, I actually bought enough for Daru-kun and Cris-chan. You think they'll eat it here? I think they're best when warm~"
	[Rintarou] "Then just save them for later."
	@input


*phone
@ifflag name="pocketty_get" value="0"
	[Rintarou] "It's me. I've got a small favor to ask. ...Yeah, sorry about that."
	[Rintarou] "It appears someone has stolen my hostage's pocket watch. Can you investigate for me?"
	[Rintarou] "...What!? You're telling me that Akihabara's black market organization's involved in all this!?"
	[Rintarou] "Tch, this is starting to look like more trouble than I thought. ...Yeah, it certainly could have already been sold."
	[Rintarou] "Looks like I've got no choice but to rush into enemy territory."
	[Rintarou] "...I don't have any choice, do I? I may already be too late."
	[Rintarou] "...No, nothing you do can stop me. I'll achieve my goal. That's Steins;Gate's choice. El Psy Congroo."
@else
	[Rintarou] "It's me. ...Yeah, I got the goods. ...No, sorry to bother you."
	[Rintarou] "Resuming mission. That's Steins;Gate's choice. El Psy Congroo."
@endif
	@input


*assistant
*call assistant

@ifflag name="pocketty_get" value="0"
	@iftemp name="joshu"
		You try calling Assistant.
		[Kurisu] "Hallo-"
		[Rintarou] "Did you find it?"
		[Kurisu] "Mayuri's watch? Still looking for it."
		[Rintarou] "The sun will set if you waste more time, you know. You'll let the enemy win...!"
		[Kurisu] "Just who the hell are you fighting...?"
		[Rintarou] "Whatever, just get to it! You need to find it before -they- do! That's an order!"
		She hung up during your rant.
		@input
	@else
		@tf name="joshu"
		You try calling Assistant.
		[Kurisu] "Hallo-"
		[Rintarou] "Assistant, where are you?"
		[Kurisu] "Lab."
		[Rintarou] "Please come out. It looks like Mayuri's lost her pocket watch."
		Kurisu soon comes out from the building.
		@draw file=cg35_d
		[Kurisu] "Mayuri, you lost your watch?"
		[Mayuri] "Yeah. I've been searching for it, but I can't find it...."
		[Rintarou] "Cristina... Please, can you help search too?"
		[Kurisu] "Of course. So, do you know where you lost it?"
		Mayuri weakly shakes her head.
		[Kurisu] "Then we just have to retrace your steps."
		[Rintarou] "Then, Assistant, I place you in charge of searching the lab!"
		[Kurisu] "Don't boss me around for every little thing."
		Despite her complaints, Kurisu returns to the lab.
		@draw file=cg35_b
		[Mayuri] "Thak you, Okarin."
		[Rintarou] "Heheheh. As a mad scientist, it is my duty to use my underlings to their fullest capacity."
		[Rintarou] "Just behold my people managing skills!"
		@input
	@endif
@else
	You try calling Assistant.
	[Kurisu] "...what."
	[Rintarou] "I got the goods. I repeat. I got the goods."
	[Kurisu] "And what are the goods? Please explain more when you talk."
	[Rintarou] "I'm talking about Mayuri's pocket watch. What else?"
	[Kurisu] "You found it?"
	[Rintarou] "Yeah. So there's no need for you to keep searching. Good work."
	[Kurisu] "That's great. Great, but, could you stop talking so haughtily?"
	[Rintarou] "Did you want the honors? Don't worry. I recognize the significance of your effort."
	[Rintarou] "Please retain your devotion to the lab. And to me. That's Steins;Gate's--"
	She already hung up.
	@input
@endif
	@input


*give clock
*give kaichu
*give kaichuu
*give kaityu
*give kaityuu
*give kaicyu
*give kaicyuu
*give pocket watch
*give watch
*give pocketty
@ifflag name="pocketty_get" value="1"
	[Mayuri] "Wawah-! It's Pocketty~san! Okarin's amazing- It's a miracle-"
	[Rintarou] "Indeed. I am the man who sees all, from past to future, after all. Finding Pockety~'s whereabouts was a simple task."
	[Rintarou] "That's right. I, and I alone, am a true insane mad scientist. Now, Mayuri, praise my name!"
	[Mayuri] "Okarin, thank you-! You know, Mayushii's glad to be Okarin's hostage."
	[Rintarou] "..."
	No, you're supposed to say "kyahh, Hououin Kyouma-sama!" or something like that....
	But Mayuri's too happy rubbing her cheeks against Pocketty~ to pay any attention to your petty complaints.
	Well, Pocketty~ -is- precious to Mayuri. Good thing you got it back.
	@flag name="osxxkufx2010" value="1"
@else
	You don't have that.
@endif
	@input


*open door
@ifflag name="lab_door_opened" value="0"
	[Rintarou] "Mayuri, do you know how to open the lab door?"
	[Mayuri] "Ahh, that door's so cute, isn't it-♪"
	In Japanese, please. The door's cute? Some sort of anthropomorphication? Or does she just like to say "cute"?
	[Mayuri] "You know, it was Mayushii's idea to make Upa-san into the key."
	The key... is an Upa...?
	[Rintarou] "Can it be any Upa?"
	[Mayuri] "Nope. It's gotta be a Metal Upa~"
	[Mayuri] "There are too many Upas on the market, you know. So we don't want just anyone getting in."
	[Mayuri] "So that's why I choce the rare Metal Upa."
	[Rintarou] "But that's just too rare, dontcha think...? Now you're the only one who can get in."
	[Mayuri] "Daru-kun's got one too~"
	They're rare! Everyone has them!
	But... hmm, yes, that's it. So Metal Upa is the key. Daru, you clever bastard!
@else
	The door's already open.
@endif
	@input


*neithart
*neithardt
*neidhart
*neidhardt
*naitoharuto
*ask neithart
*ask neithardt
*ask neidhart
*ask neidhardt
*ask naitoharuto
*ask about neithart
*ask about neithardt
*ask about neidhart
*ask about neidhardt
*ask about naitoharuto
*ask about knight-hart
@ifflag name="osxxkufx2010" value=1 name="knightheart_1" value="1"
	[Rintaru] "Mayuri. Have you seen Neithardt?"
	[Mayuri] "Ehh-? Night Heart? Is that a candy? Or, a pony? Or, an RPG character?"
	[Rintarou] "Can you think of nothing besides food and ota junk? And you call yourself a high school girl!?"
	[Mayuri] "None of those? Well, then Mayuri doesn't really know."
	[Rintarou] "He's a high school boy carrying around a big cardboard box. Seen him?"
	[Mayuri] "Ah, ahh-! I -have- seen that guy-"
	[Rintarou] "R-really!?"
	What luck!
	[Rintarou] "So, where exactly did you see him?"
	[Mayuri] "Hmmm, before Mayushii came here, I saw him at animate."
	[Mayuri] "You know, he was carrying that giant cardboard box, as well as tons of paper bags from animate- He looked like he was coming home from ComiMa- Ehheheh-"
	How the hell is he moving around so much with that 25 kg IBN 5100 in his arms?
	That's not humanly possible, is it? Normally, that would restrict movement.
	So he's probably still somewhere in Akihabara, then.
	[Rintarou] "Thanks, Mayuri! Next time, I'll give you three bottles of Dr Pepper! Fuhahaha!"
	[Mayuri] "Ehh-- But Mayushii's not good with carbonated drinks-"
	@flag name="knightheart_3" value="1"
@else
	@ifflag name="oxxxkuxx2010" value="1"
		[Rintaru] "Mayuri. Have you seen Neithardt?"
		[Mayuri] "Ehh-? Night Heart? Is that a candy? Or, a pony? Or, an RPG character?"
		[Rintarou] "Can you think of nothing besides food and ota junk? And you call yourself a high school girl!?"
		[Mayuri] "None of those? Well, then Mayuri doesn't really know."
		Hmm, guess there's no point in asking unless you have some more concrete details about Neithardt.
	@else
		What?
	@endif
@endif
	@input

*d mail
*time leap
	You can't do that in this world line.
	@input

*take mayuri
*get mayuri
	Mayuri's already lab property.
	@input

*kiss
*kiss mayuri
*chu chu
*chu
*chu mayuri
	No thanks.
	@input


*punch
*punch mayuri
*kick
*kick mayuri
	Don't be ridiculous!!!
	@input



*sit bench
	Hup.
	@input

