
@ifflag name="lab_door_opened" value="1"
	@draw file=cg36_c
	@play file=bgm03
@else
	@ifflag name="uupa_set" value="1"
		@draw file=cg36_b
	@else
		@draw file=cg36_a
	@endif
@endif


@ifflag name="osxxkufx2010" value="0"
	Let's head upstairs to the lab.
	[Mayuri] "Okariiin, let's look for Pocketty~..."
	You hear Mayuri's sad voice from below.
	Tch. Looks like you've got no choice. You're busy, but you just can't refuse a request from Mayuri.
	You take a U-turn.
	@stop
	@clear
	@goto file=SG5-1a
@endif

@ifflag name="lab_door_opened" value="1"
	You insert the key, the Metal Upa, unlocking the door.
	This lock system's such a pain in the ass....
	@input
@endif

@ifflag name="osxxkufx2010" value="1" name="lab_door_opened" value="0" name="flag5_2a_01" value="0"
	@flag name="flag5_2a_01" value="1"
	You head upstairs to the lab.
	The lab. As in the Future Gadget Laboratory. Not that it says that on the door, or anything.
	And, even though you know this door better than your house door, you feel something very wrong about it.
	When did the lab door look like this? There's something off about the color, too.
	You know this door well. But you've never paid this much attention to it.
	So, you don't really quite know what seems different about it.
	No, there is -one- thing that you know is different for sure.
	Right near the doorknob. There's an unfamiliar circular indentation there.
	@input
@endif

@ifflag name="osxxkufx2010" value="1" name="lab_door_opened" value="0" name="flag5_2a_01" value="1"
	You head upstairs to the lab.
	But the mysterious door is tightly sealed. You cannot get in.
	You need to think of a way to unlock this door.
	Because you're worried for Assistant, who's still inside. She could get assaulted by Perv Meister Daru.
	[Rintarou] "Daru! Contain yourself-!"
	@input
@endif
	@input

*u
*l
*up
*left
	That's the staircase upstairs. No real point in going there.
	@input


*r
*right
	You're a little too old to creep against the wall and play ninja.
	@input

*d
*b
*down
*back
	@stop
	@clear
	@goto file=SG5-1a


*f
*front
*forward
@ifflag name="lab_door_opened" value="0"
	You try turning the doorknob, but it doesn't open.
@else
	@stop
	@clear
	@goto file=SG5-3a
@endif
	@input

*look
*look door
@ifflag name="uupa_set" value="1"
	There's an Upa set into the door.
	@input
@endif

@ifflag name="lab_door_opened" value="0"
	The indentation in the door is about 10 cm in diameter, and 5 cm deep.
	There's some sort of crazy wiring going on inside....
	Just what the hell is this?
	Looks like you're supposed to insert some sort of capsule-shaped object, but what?
@else
	The door's unlocked.
@endif
	@input


*open door
@ifflag name="lab_door_opened" value="0"
	[Rintarou] "Open sesame."
	You try saying the magic words, but the door remains unopened. ...How embarrassing.
@else
	[Rintarou] "Open, Steins;Gate!"
	You slowly open the door, and head inside.
	@stop
	@clear
	@goto file=SG5-3a
@endif
	@input

*push
*push door
@ifflag name="lab_door_opened" value="0"
	If pulling's no good, then try pushing.
	Well, it was worth a try.
@else
	The door's already open.
@endif
	@input

*pull
*pull door
@ifflag name="lab_door_opened" value="0"
	If pushing's no good, then try pulling.
	Well, it was worth a try.
@else
	The door's already open.
@endif
	@input


*punch door
*attack door
@ifflag name="lab_door_opened" value="0"
	[Rintarou] "Hououin Kyouma Final Esoterica Type 12... Rising Flarewing Fist!"
	You throw a punch with all your might.
	[Rintarou] "OUCH! FUCK! SHIT! Gwahhh"
	It was pretty solid. You underestimated it....
	[Itaru] "Hey, Okarin? Don't be so rough with it."
	Daru yells out to you from across the door.
@else
	The door's already open.
@endif
	@input


*kick door
@ifflag name="lab_door_opened" value="0"
	[Rintarou] "Hououin Kyouma Final Esoterica Type 106... Noble Flarewind Kick!"
	You throw a kick with all your might.
	[Rintarou] "Oggh...! Urghah!"
	But the only damage was to your own foot. You crumble over, and groan in pain.
	The door doesn't even budge. It's way too sturdy....
	[Itaru] "Hey, is that you, Okarin? Did you just kick it? Cut it out! Dye!"
	Daru yells out to you from across the door.
@else
	The door's already open.
@endif
	@input


*use key
@ifflag name="lab_door_opened" value="0"
	Don't know if your old spare key will work or not, but it's worth at least a shot.
	But the doorknob has no keyhole.
	Is it locked by something other than a key?
@else
	The door's already open.
@endif
	@input


*call daru
*call hashida
*call itaru
@ifflag name="lab_door_opened" value="0"
	You try calling out to the other side of the door.
	[Rintarou] "Hey, Daru! I know you're in there! Open up the door! Right now, within 30 seconds!"
	[Rintarou] "If you refuse, then... sorry, but I'll have to unseal my right arm...."
	[Rintarou] "And you know what that means, don't you? I'll blow down not just this door, but this entire building...."
	[Rintarou] "I'm serious. So just open the door before you do something you'll regret."
	[Itaru] "Sorry, no can do. Busy with an eroge here."
	[Rintarou] "What is this, I don't even..."
	Daru's title changed to "Perv Meister".
@else
	You try calling out to the other side of the door.
	[Itaru] "Sorry, no can do. Busy with an eroge here."
	[Rintarou] "What is this, I don't even..."
	Daru's title changed to "Perv Meister".
@endif
	@input


*phone
@ifflag name="lab_door_opened" value="0"
	[Rintarou] "It's me.... What? I sound tired? Well, yeah, I guess I am."
	[Rintarou] "I'm honestly bewildered here. ...Yeah, looks like My Favorite Right Arm has betrayed me."
	[Rintarou] "Hmph, yeah, I know...."
	[Rintarou] "So in the end, can nobody understand the insane mad scientist way of life?"
	[Rintarou] "Nevertheless, I won't allow betrayal of any sort. ...Yeah, I'll settle this with Daru myself."
	[Rintarou] "That's Steins;Gate's choice. El Psy Congroo."
@else
	[Rintarou] "It's me. It's a trap. I repeat. It's a trap. Proceeding with caution."
	[Rintarou] "...Yeah, exactly. I won't allow betrayal of any sort. Even betrayal by mine own right arm."
	[Rintarou] "That's Steins;Gate's choice. I'll call back. El Psy Congroo."
@endif
	@input


*assistant
*call assistant
@ifflag name="lab_door_opened" value="1"
	You try calling Assis... no, you don't. She's probably inside, so there's no point.
	@input
@endif

@iftemp name="joshu"
	You try calling Assistant.
	[Rintarou] "..."
	Why won't she pick up!? She must have some nerve to ignore you when she's your assistant, eh!?
	@input
@else
	@tf name="joshu"
	You try calling Assistant.
	[Rintarou] "It's me. Assistant. Are you okay?"
	[Kurisu] "What do you mean?"
	[Rintarou] "I'm asking if you've fallen pray to Daru's dark talons!"
	[Kurisu] "If you're talking about Hashida, he's in the middle of some eroge."
	[Rintarou] "I see. Great to hear you're okay. So, Assistant. Sorry, but can you please open the door?"
	[Kurisu] "Ahh, oh yeah. The door got replaced. So the keys changed."
	[Rintarou] "And the key?"
	[Kurisu] "He said it's that thing that recently got famous in Akiba. I don't really get it, so why not ask Mayuri about it? She's probably an expert on the subject."
	[Rintarou] "Whatever, just open the door!"
	[Kurisu] "I can't. I don't have a key either. I got Hashida to open it for me earlier."
	[Rintarou] "You need a key to open it from the inside?"
	[Kurisu] "Apparently."
	[Rintarou] "Damn you Daru...! Just what the hell's his problem!? Assistant, whatever you do, don't move from there, okay!? I'll be sure to save you!"
	[Kurisu] "Uh- why are you so frantic? It's not like I'm in any danger or anything."
	[Kurisu] "...Ah, no, let me rephrase that. I mean, you are worrying about me after all."
	[Kurisu] "Thanks. You really do care about me, don't you? That makes me really hap--"
	You hang up and try to think of ways to open the door.
	Assistant was muttering something or another, but you didn't quite catch what it was.
	@input
@endif
	@input

*set upa
*put upa
*place upa
*use upa
*insert upa
@ifflag name="uupa_set" value="1"
	There's already an Upa set into the door.
	@input
@endif
@ifflag name="lab_door_opened" value="0" name="upa_get" value="1"
	@draw file=cg36_b
	You insert the Metal Upa into the indentation on the door.
	@flag name="uupa_set" value="1"
	Wait- was this really the key item to opening Steins;Gate...!?
	[Rintarou] "..."
	But nothing really happens. Even after you wait for a while.
	Is inserting it not enough?
@else
	What?
@endif
	@input




*turn upa
@ifflag name="uupa_set" value="1"
	@play file=bgm03
	@se file=se02 ch=0 num=0
	@draw file=cg36_c
	There's a nice click sound, which continues into various other machine noises. And then the door opens.
	@flag name="lab_door_opened" value="1"
@else
	What?
@endif
	@input
