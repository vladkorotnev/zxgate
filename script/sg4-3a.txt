
@play file=bgm03

@ifflag name="frog_ensmallened" value="0"
	@draw file=cg32_a
	MayQueen+Nyan2 is a cat-eared maid cafe. All of the girls working here wear cat ears.
	Is that an original twist, or a heresy to the concept? The great debate has been endless, ever since the store opened.
	Well, whatever. The biggest issue is--
	[Rintarou] "What the hell is that frog--!?"
	There's a giant stuffed animal in front of the store. It's that "gero froggy" thing that was once popular with Shibuyan high school girls.
	It's right in front of the store. As in, it's blocking the way.
@else
	@draw file=cg32_b
	You're a regular here at the maid cafe, MayQueen+Nyan2.
	Though, you just come here for the food. Not to chat with the maids.
	Your motives aren't as pure as Daru's. Nor are you overly obstinant.
	You're just the right amount of corrupted....
@endif
	@input


*b
*back
	There are cars zipping back and forth. You should try to avoid any actions that could end in you getting run over to death.
	Death is always by your side. You're always in its grasp. So try to live, as long as you're alive.
	Your future self... would probably agree.
	You close your eyes halfway, and comb back your hair with a sigh.
	@input


*l
*left
	@stop
	@clear
	@goto file=SG4-2a


*r
*right
	You have no intention to go to another maid cafe.
	@input


*u
*up
*f
*front
*forward
@ifflag name="frog_ensmallened" value="0"
	[Rintarou] "Outta my wayyyyy!"
	You stamp your foot to intimidate the gero froggy. But he remains undaunted. You cannot enter the store!
@else
	@stop
	@clear
	@goto file=SG4-4a
@endif
	@input


*look
@ifflag name="frog_ensmallened" value="0"
	There's a giant frog in front of MayQueen+Nyan2.
	For now, you cannot enter the store unless you do something about it.
	But that massive frog is powerless against the power of future science.
	If only you had something like that flashlight Braun had....
@else
	The nuisance has been dealt with.
	Frogs don't really match MayQueen+Nyan2 anyway.
	Hmph. Yet another boring task.
@endif
	@input

*use light
*use flashlight
@ifflag name="frog_ensmallened" value="0" name="another_flashlight_get" value="1"
	[Rintarou] "There's a flashlight for that."
	The flashlight you got underneath the girder bridge. The one that said "Small�".
	You turn it toward the giant gero froggy, and turn on the switch.
	[Rintarou] "Oh, oh ohhhhhh..."
	@se file=se01 ch=0 num=0
	@draw file=cg32_b
	You can't help but gasp in awe at the unreal sight before your eyes.
	The gero froggy literally shrinks. Down to a pinky size.
	And as soon as that happens, the flashlight's bulb explodes.
	[Rintarou] "Wha!? It broke!? Don't tell me it was a one-use item..."
	Good thing you didn't waste it on something stupid. Whew....
	@flag name="frog_ensmallened" value="1"
	@input
@endif

@ifflag name="frog_ensmallened" value="1" name="another_flashlight_get" value="1"
	The flashlight's bulb exploded. It's no longer usable.
@else
	You don't have that.
@endif
	@input


*look frog
*look gerokaerun
*look gero froggy
@ifflag name="frog_ensmallened" value="0"
	What the hell was with the gero froggy boom, anyway?
	Why'd such a half-assed character get that explosively popular?
	Nobody with any brains can explain it. At least, not officially.
	Truth is, that boom has now been elevated to the level of urban legend.
	Actually, forget about that! Right now, it's just in your way. Why the hell is it placed in front of the store!?
	Is it supposed to attract prospective customers in the signboard's stead, or something?
	Yeah right. Those prospective customers can't even get in.
@else
	There's a gero froggy at the bottom of the stairs. It's tiny.
	The doll used to be 3 meters high, but now it's tiny.
	And yet, it lost none of its detail. It's still a perfectly shaped gero froggy.
	It literally just scaled down. How is that even physically possible...?
	Is it an abnormality of the world? Or an abnormality of the flashlight?
	Anyway, at the very least, it's abnormal.
@endif
	@input


*look signboard
*look sign
@ifflag name="frog_ensmallened" value="0"
	There's some round handwriting on the fallen signboard.
	"Gero Froggy Meowmorial Dayo"
	Oh god, that pun....
@else
	The frog is gone.
@endif
	@input


*hit frog
*hit gerokaerun
*hit gero froggy
@ifflag name="frog_ensmallened" value="0"
	Ribbit!
@else
	The frog is gone.
@endif
	@input


*punch frog
*punch gerokaerun
*punch gero froggy
@ifflag name="frog_ensmallened" value="0"
	Rrirribbit!
@else
	The frog is gone.
@endif
	@input


*attack frog
*attack gerokaerun
*attack gero froggy
@ifflag name="frog_ensmallened" value="0"
	Rrrrrrrrrrrrribbit!
@else
	The frog is gone.
@endif
	@input


*kick frog
*kick gerokaerun
*kick gero froggy
@ifflag name="frog_ensmallened" value="0"
	Ri-bit!
@else
	The frog is gone.
@endif
	@input


*phone
@ifflag name="frog_ensmallened" value="0"
	[Rintarou] "It's me. Looks like the one-year-dead spirit has revived."
	[Rintarou] "...Yeah. That frog. Furthermore, it's absorbed all the hatred in the world, and grown in size. Leave it be, and it may literally crush Akihabara."
	[Rintarou] "...No, of course not. The Organization's brainwashing was all undone on that 'Day of Ruin'."
	[Rintarou] "How am -I- going to purify it? ...Hmph. So, am I the only one who does any work here?"
	[Rintarou] "...Okay. I'll do something. For now, all I have to do is 'ensmallen' the frog back to its original size, right?"
	[Rintarou] "This too is Steins;Gate's choice. El Psy Congroo."
@else
	[Rintarou] "It's me. ...Yeah, I'm okay. There's no way a frog can beat me. Shouldn't you of all people know that?"
	[Rintarou] "...That's right. Cats are more of a bother than frogs."
	[Rintarou] "Is this also Steins;Gate's choice...? El Psy Congroo."
@endif
	@input


*assistant
*call assistant
@ifflag name="frog_ensmallened" value="0"
	You try calling Assistant.
	[Rintarou] "Assistant, have you seen Mayuri today?"
	[Kurisu] "I'm at the lab right now, but she's not here. Isn't she at work?"
	[Kurisu] "Well, at least I thought she'd be, but according to Hashida, Mayuri didn't go to work today."
	[Kurisu] "Why does Hashida know that much?"
	[Rintarou] "That man has a complete understanding of MayQueen's weekly work schedule. For all maids."
	[Kurisu] "That's realy creepy."
	[Rintarou] "Agreed. By the way, do you like giant stuffed animals?"
	[Kurisu] "Eh? Why? D-don't tell me... you're giving me a present?"
	[Kurisu] "I-it's already long past my birthday, and it's nowhere near Christmas or Valentine's, so, why?"
	[Kurisu] "W-well, the occassion doesn't matter. It's the thought that counts, so, I guess I'll take it."
	She's making a grave misunderstanding, but maybe you can use that to your advantage.
	[Rintarou] "Then meet me in front of MayQueen right away."
	[Kurisu] "In front of MayQueen? Well, okay..."
	--A few minutes later.
	@draw file=cg32_c
	[Kurisu] "That's kinda creepy."
	[Rintarou] "Yeah. Real creepy."
	You and Kurisu stand in front of the giant gero froggy, and utter words of agreement.
	[Kurisu] "We finally agree on something. Currently, our agreement percentage is a single digit value."
	[Rintarou] "You're keeping track?"
	[Kurisu] "Just an estimate. I don't have that much free time, and it's not like I care about you, or anything."
	[Rintarou] "Assistant. This giant gero froggy is a present of my love for you."
	[Kurisu] "Who would want this-!? How can you even bring it home!?"
	[Kurisu] "...wait, huh? L-love? H-hey, you...!"
	She starts to get flustered in a delayed reaction. Hey. Assistant. Your cheeks are bright red, you know?
	[Rintarou] "So, with that said- Take it away!"
	[Kurisu] "But, that's physically impossible.... Psychologically... I'm happy, though."
	[Rintarou] "If you can't take it away, then I don't need you anymore! Forget about the present!"
	[Kurisu] "...hey. Was this some dumb trick of yours again?"
	Oh shi-
	Kurisu's expression changes. You probably shouldn't have said that.
	[Kurisu] "Statistically speaking, this frog probably isn't yours, now, is it?"
	You probably shouldn't shoot your foot with any more lies. You get an idea.
	[Rintarou] "Sensei, I want... to move this doll out of the way...."
	[Kurisu] "I'm starting to hate myself for being so gullible."
	Kurisu sighs and tries to go away. You hurriedly hold her back.
	[Rintarou] "Please, wait! Let's think of a way to get this giang frog out of the way."
	[Kurisu] "Why don't you go ask a Future Gadget cat?"
	[Rintarou] "Hahah. What a funny and relevant Japanese pop-culture reference!"
	[Kurisu] "Oh, shut up."
	[Kurisu] "At least it wasn't as stupid as foisting giant stolen goods onto me."
	@draw file=cg32_a
	Kurisu sighs, and leaves.
	Hmm, a Future Gadget cat.... This would be a piece of cake if you only had his secret tools.
@else
	You try calling Assistant.
	[Kurisu] "Hallo..."
	[Rintarou] "Assistant. Allow me to reward you for all your hard work. How about we have a meal at a cafe?"
	[Kurisu] "Wh-what's this all of a sudden? What are you scheming this time?"
	[Kurisu] "I just want to express my true feelings of gratitude towards you. I wouldn't be where I am if I didn't have you."
	[Kurisu] "Okabe.... W-well, I guess I -am- hungry. Sure. I'll take you up on your offer."
	[Kurisu] "So, where to?"
	[Rintarou] "MayQueen+Nya--"
	She hangs up before you can finish.
	Dammit! Does she really hate cat-ear maid cafes that much!? Damn selfish assistant!
@endif
	@input


*neithart
*neithardt
*neidhart
*neidhardt
*naitoharuto
*ask neithart
*ask neithardt
*ask neidhart
*ask neidhardt
*ask naitoharuto
*ask about neithart
*ask about neithardt
*ask about neidhart
*ask about neidhardt
*ask about naitoharuto
*ask about knight-hart
@ifflag name="ZX_Spectrum" value="1"
	Oh yeah. Let's have Faris gather information about Neithardt, too. You feel a little awkward approaching the store, but....
@else
	What?
@endif
	@input

*d mail
*time leap
	You can't do that in this world line.
	@input

*kiss gerokaerun
*kiss frog
*chu frog
*chu gerokaerun
*kiss gero froggy
*chu gero froggy
	You'll get cursed.
	@input

*get frog
*get gerokaerun
*take frog
*take gerokaerun
*get gero froggy
*take gero froggy
	You really don't want this thing.
	@input

*kill frog
*kill gerokaerun
*kill gero froggy
	You can't kill the unliving.
	@input

*hug frog
*hug gerokaerun
*hug gero froggy
	It looks slippery. I wouldn't advise it.
	@input

*eat frog
*eat gerokaerun
*eat gero froggy
	They -do- say frogs taste like chicken. But there's just too much to eat.
	Plus, it doesn't look all that tasty.
	@input


*burn frog
*burn gerokaerun
*burn gero froggy
	Not very effective!!
	Water types resist fire.
	@input
