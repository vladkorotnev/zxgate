#!/bin/env python

import sys
from os.path import basename, splitext
from os import fstat
from zxtools import *
from math import ceil

fileMap = []
filesToProcess = []
outImageName = ""

outImageName = sys.argv[1]
outImageNum = 0

for ai in range(2, len(sys.argv)):
		filesToProcess.append(sys.argv[ai])

ofname = splitext(basename(outImageName).upper())[0]+str(outImageNum)+".C"
fout = open(ofname, "wb")
curImgPos = 0
curSectPos = 0

print "+ Writing volume to ", ofname

for curFile in filesToProcess:
	fin = open(curFile, "rb")
	fbuf = fin.read()
	flen = fstat(fin.fileno()).st_size
	fsect = int(ceil(flen / float(CONST_TRDOS_SECTOR_SIZE)))
	flen_pad = fsect * CONST_TRDOS_SECTOR_SIZE
	fn = filenameToTRDOS(splitext(basename(curFile))[0])
	if (curImgPos + flen_pad > CONST_TRDOS_MAX_FILE_SIZE):
		fout.close()
		outImageNum += 1
		ofname = splitext(basename(outImageName).upper())[0]+str(outImageNum)+".C"
		fout = open(ofname, "wb")
		curImgPos = 0
		curSectPos = 0
		print "+ Writing volume to ",ofname
	curFileDescriptor = (fn, curSectPos, fsect, outImageNum)
	print "-> ",fn,"start sector",curSectPos,"; total sectors",fsect,"; volume",outImageNum
	fileMap.append(curFileDescriptor)
	fout.write(fbuf)
	for i in range(0, flen_pad - flen ):
		fout.write(chr(0x00)) # padding to sector
	curImgPos += flen_pad
	curSectPos += fsect
	fin.close()

fout.close()

fmname = splitext(basename(outImageName).upper())[0]+"_.C"
fmout = open(fmname, "w")
print "+ Writing file map to ",fmname

for desc in fileMap:
	# write 9 bytes file name
	fmout.write(desc[0])
	fmout.write(chr(0x00)) # null terminated
	# write 1 byte volume number
	fmout.write(chr(desc[3] & 255))
	# Write 1 byte start sector
	fmout.write(chr(desc[1] & 255))
	# Write 1 bytes of Length, in sectors 
	fmout.write(chr(desc[2] & 255))


fmout.write(chr(255)) # 0xFF terminator

fmout.close()

print "+ Done"
