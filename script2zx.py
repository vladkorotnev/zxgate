#!/bin/env python

import sys
from os.path import basename, splitext
from zxtools import filenameToTRDOS
import textwrap

flags = ["__do_not_use_me_temp_fix_for_issue2__"]
sysflags = []
tempflags = []


filesToProcess = []

	
def preprocessForCustomChars(line):
	ltemp = line.replace("\x83\xBF","\x90").replace("\x83\xC0","\x91").replace("\x81\x99","\x99").replace("\x81\xF4","\x94")
	return "".join([ch for ch in ltemp if (ord(ch) <= 164 and ord(ch) >= 20)]) # remove all chars out of print range

def mkFlag(name):
	flags.append(name)
	if len(flags) > 211:
		print "FATAL: Flag count exceeded 211, aborting build..."
		sys.exit()
	print "NOTICE: New flag ",name," assigned offset ",("%0.2X" %flags.index(name))

def TFMask(name):
	if not name in tempflags:
		mkTF(name)
	return pow(2,tempflags.index(name))
def SFMask(name):
	if not name in sysflags:
		mkSF(name)
	return pow(2,sysflags.index(name))

def mkTF(name):
	tempflags.append(name)
	if len(tempflags) > 8:
		print "FATAL: Tempflag count exceeded 8, aborting build..."
		sys.exit()
	print "NOTICE: New tempflag ",name," assigned number ",("%0.2X" % TFMask(name))

def mkSF(name):
	sysflags.append(name)
	if len(sysflags) > 8:
		print "FATAL: Sysflag count exceeded 8, aborting build..."
		sys.exit()
	print "NOTICE: New sysflag ",name," assigned offset ",("%0.2X" % SFMask(name))

noEOF = False
padToScreen = False

for ai in range(1, len(sys.argv)):
	if sys.argv[ai][:2] == "--":
		if sys.argv[ai] == "--no-eof":
			print "+ Compiling without EOF mark"
			noEOF = True
		elif sys.argv[ai] == "--pad-lines":
			print "+ Word wrap build enabled"
			padToScreen = True
	else:
		filesToProcess.append(sys.argv[ai])

for fn in filesToProcess:
	print "+ Processing ",fn
	flines = [line.rstrip('\n').rstrip('\r').lstrip() for line in open(fn)]
	fout = open(splitext(basename(fn).upper())[0]+".C", "wb")
	isInCondition = 0
	tempflags = []
	cline = 0
	while cline < len(flines):
		line  = flines[cline]
		if len(line) == 0:
			cline += 1
			continue
		elif line[0] == ";":
			cline += 1
			continue
		elif line[0] == "@":
			# process command
			comdata = line[1::].split(' ')
			command = comdata[0].lower().strip()
			comarg = {}
			
			for ca in range(1, len(comdata)):
				arg = comdata[ca].strip().split('=')
				if len(arg) > 1:
					if arg[0].lower() in comarg or command == "ifflag" or command == "iftemp" or command == "ifsys":
						# Such arg already known, turn into array
						if not (arg[0].lower() in comarg) or not isinstance(comarg[arg[0].lower()], list):
							if arg[0].lower() in comarg:
								temp = comarg[arg[0].lower()]
								comarg[arg[0].lower()] = [temp]
							else:
								comarg[arg[0].lower()] = []
						comarg[arg[0].lower()].append(arg[1].strip('"'))
					else:
						comarg[arg[0].lower()] = arg[1].strip('"')
				
			if command == "stop":
				fout.write("\xF4")
			elif command == "draw" or command == "redraw":
				fout.write("\xE2")
				fout.write(filenameToTRDOS(comarg['file']))
				fout.write("\x00");
			elif command == "clear":
				fout.write("\xF6")
			elif command == "wait":
				wtime = int(comarg["time"])
				if wtime > 100:
					wtime /= 1000 # sometimes it's specified in milliseconds
				if wtime > 0 and wtime < 255:
					fout.write("\xE5")
					fout.write(chr(wtime))
				elif wtime > 255:
					print "WARNING: time ",wtime, "more than one byte, ignoring"
			elif command == "play": 
				fout.write("\xE1")
				fout.write(filenameToTRDOS(comarg['file']))
				fout.write("\x00")
			elif command == "goto":
				fout.write("\xE0")
				fout.write(filenameToTRDOS(comarg['file']))
				fout.write("\x00")
			elif command == "se":
				print "TODO: implement command @se"
			elif command == "gameover":
				fout.write("\xE4")
			elif command == "else":
				if isInCondition == 0:
					print "FATAL: unexpected @else without @ifflag/@iftemp/@ifsys. Aborting..."
					sys.exit()
				else:
					fout.write("\xF2")
			elif command == "endif":
				if isInCondition == 0:
					print "FATAL: unexpected @endif without @ifflag/@iftemp/@ifsys. Aborting..."
					sys.exit()
				else:
					fout.write("\xF3")
					isInCondition -= 1
			elif command == "ifflag":
				# In this command comargs are always arrays
				if(len(comarg['name']) != len(comarg['value'])):
					print "FATAL: unmatched count of name= and value= in @ifflag. Aborting..."
					sys.exit()
				fout.write("\xD1") # IF
				for i in range(len(comarg['name'])):
					flagname = comarg['name'][i]
					flagval = int(comarg['value'][i])
					if flagval < 0 or flagval > 255:
						print "FATAL: flag value exceeds byte range in @ifflag. Aborting..."
						sys.exit()
					if not flagname in flags:
						mkFlag(flagname)
					fout.write(chr(flags.index(flagname)))
					fout.write(chr(flagval))
				fout.write("\x00")
				isInCondition += 1
			elif command == "iftemp":
				fout.write("\xD3")
				tm = 0
				for fln in comarg['name']:
					tm = tm | TFMask(fln)
				fout.write(chr(tm))
				isInCondition += 1
			elif command == "ifsys":
				fout.write("\xD5")
				tm = 0
				for fln in comarg['name']:
					tm = tm | SFMask(fln)
				fout.write(chr(tm))
				isInCondition += 1
			elif command == "flag":
				flagname = comarg['name']
				flagval = int(comarg['value'])
				if flagval < 0 or flagval > 255:
					print "FATAL: flag value exceeds byte range in @flag. Aborting..."
					sys.exit()
				if not flagname in flags:
					mkFlag(flagname)
				fout.write("\xD0")
				fout.write(chr(flags.index(flagname)))
				fout.write(chr(flagval))
			elif command == "tf":
				fout.write("\xD2")
				fout.write(chr(TFMask(comarg['name'])))
			elif command == "sf":
				fout.write("\xD4")
				fout.write(chr(SFMask(comarg['name'])))
			elif command == "save":
				fout.write("\xF0")
				fout.write(comarg['file'])
				fout.write("\x00")
			elif command == "load":
				fout.write("\xF1")
				fout.write(comarg['file'])
				fout.write("\x00")
			elif command == "if":
				print "FATAL: unsupported command @if. Use @ifflag name=\"\" value=\"\", @iftemp name=\"\" and @ifsys name=\"\" instead. Aborting..."
				sys.exit()
			elif command == "set":
				print "FATAL: unsupported command @set. Use @flag name=\"\" value=\"\", @tf name=\"\" and @sf name=\"\" instead. Aborting..."
				sys.exit()
			elif command == "input":
				fout.write("\xFF")
			else:
				print "WARNING: Don't know command @",command, comarg
			cline += 1
		elif line[0] == "*":
			#print "TODO: implement * Pointer"
			fout.write("\xFD") #start of pointer list
			ptr = []
			while len(line) > 0 and line[0] == "*":
				ppap = line.lstrip("*").lower()
				if len(ppap) > 32:
					print "WARNING: pointer ",ppap," is more than 32 chars! Ignored."
				else:
					ptr.append(ppap)
				cline += 1
				line = flines[cline]
			#print "NOTICE: pointers ",ptr
			for p in ptr:
				fout.write(preprocessForCustomChars(p))
				fout.write("\x00")
			fout.write("\xFE")
		else:
			if padToScreen:
				paddedLine = ""
				# do some word wrapping preprocessing
				paddedLineArray = textwrap.wrap(line, 32)
				for x in paddedLineArray:
					if paddedLineArray[-1] == x:
						paddedLine += x # don't pad last line
					else:
						paddedLine += '{:<32}'.format(x)
				fout.write(preprocessForCustomChars(paddedLine))
				fout.write("\x00")
			else:
				fout.write(preprocessForCustomChars(line))
				fout.write("\x00")
			cline += 1
	if not noEOF:
		fout.write("\xF7") #EOF mark
			
print "+ Done. Compiled",len(filesToProcess)," files, with ",len(flags)," flags and ",len(sysflags)," sysflags."